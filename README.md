# The Isotope Project

`isotope` is a prototype intermediate representation (IR) aiming to support dependently typed verification of Rust-like programs. To do so, we equip an [RVSDG](https://arxiv.org/abs/1912.05036)-like IR with a system of [linear dependent types](https://www.cl.cam.ac.uk/~nk480/dlnl-paper.pdf) extended with a notion of [borrowing](https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html).

This repository consists of a Rust implementation of the `isotope` IR and various utilities and example programs, all of which is still a very early work in progress. Compiling and running these examples requires the latest stable version of Rust (earlier versions *may* work); you can then run the tests using `cargo test`.

Questions and contributions are highly appreciated: please open an issue or pull request, or e-mail the author at <jad.ghalayini@cs.ox.ac.uk>.
/*!
The main repl struct
*/
use super::*;
use isotope_lang::builder::Builder;

/// A repl command
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// An `isotope` statement
    Statement(Statement),
    /// An `isotope` term
    Term(Term),
    /// Typecheck a term
    Check(Term),
}

/// Attempt to parse a repl command
pub fn command(input: &str) -> IResult<&str, Command> {
    alt((
        map(stmt, Command::Statement),
        map(term, Command::Term),
        map(preceded(preceded(tag("#check"), ws), term), Command::Check),
    ))(input)
}

/// The repl state
#[derive(Default)]
pub struct Repl {
    /// Whether to debug parse states
    pub debug_parse: bool,
    /// The builder associated with this repl
    pub builder: Builder,
}

impl Repl {
    /// Create a new repl
    pub fn new() -> Repl {
        Repl::default()
    }
    /// Handle a line as input to the repl
    pub fn handle_line<'a>(&mut self, line: &'a str) -> IResult<&'a str, ()> {
        let (rest, command) = command(line)?;
        println!("Parsed command: {:#?}", command);
        match &command {
            Command::Statement(stmt) => match self.builder.stmt(stmt) {
                Ok(()) => {}
                Err(err) => println!("Error processing statement: {}", err),
            },
            Command::Term(term) => match self.builder.term(term) {
                Ok(value) => println!("Built value: {:#?}", value),
                Err(err) => println!("Error building value: {}", err),
            },
            Command::Check(term) => match self.builder.term(term) {
                Ok(value) => println!("{}", value.tyck()),
                Err(err) => println!("Error building value: {}", err),
            },
        }
        println!("Leftover input: {}", rest);
        Ok((rest, ()))
    }
}

use anyhow::Error;
use clap::{App, Arg};
use isotope::term::Value;
use isotope_lang::ast::*;
use isotope_lang::parser::*;
use nom::{branch::alt, bytes::complete::tag, combinator::*, multi::*, sequence::*, IResult};
use rustyline::error::ReadlineError;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::{Context, Editor};
use rustyline_derive::{Completer, Helper, Highlighter};

mod helper;
mod repl;
use helper::*;
use repl::*;

const HISTORY: &str = ".isotope_history";
const PROMPT: &str = ">>> ";

fn main() -> Result<(), Error> {
    let matches = App::new("isotope repl")
        .version("0.0.0")
        .author("Jad Ghalayini")
        .about("A read-evaluate-print-loop for experimenting with isotope the isotope intermediate representation")
            .arg(Arg::with_name("history")
            .short("h")
            .long("history")
            .value_name("FILE")
            .help("The file to load history from/store history to. Defaults to .isotope_history")
            .takes_value(true))
        .get_matches();
    let mut editor = Editor::<IsotopeValidator>::new();
    let history = matches.value_of("history").unwrap_or(HISTORY);
    if editor.load_history(history).is_err() {
        eprintln!("No previous history loaded.")
    }
    let validator = IsotopeValidator::new();
    editor.set_helper(Some(validator));
    let mut repl = Repl::new();
    loop {
        match editor.readline(PROMPT) {
            Ok(line) => match repl.handle_line(&line) {
                Ok(_) => {
                    editor.add_history_entry(&line);
                }
                Err(err) => eprintln!("REPL error: {}", err),
            },
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
            Err(err) => eprintln!("IO error: {:#?}", err),
        }
    }
    editor.save_history(HISTORY)?;
    Ok(())
}

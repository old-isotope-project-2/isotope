/*!
A helper for the `isotope` repl
*/
use super::*;

/// A validator for `isotope` input code.
#[derive(Completer, Helper, Highlighter)]
pub struct IsotopeValidator {
    hinter: HistoryHinter,
}

impl IsotopeValidator {
    pub fn new() -> IsotopeValidator {
        IsotopeValidator {
            hinter: HistoryHinter {},
        }
    }
}

impl Validator for IsotopeValidator {
    fn validate(&self, ctx: &mut ValidationContext) -> Result<ValidationResult, ReadlineError> {
        match terminated(many0_count(command), opt(ws))(ctx.input()) {
            Ok(("", _n)) => Ok(ValidationResult::Valid(None)),
            Ok((_rest, _n)) => Ok(ValidationResult::Incomplete),
            Err(err) => Ok(ValidationResult::Invalid(Some(format!(
                "Parse error: {:#?}",
                err
            )))),
        }
    }
}

impl Hinter for IsotopeValidator {
    type Hint = String;
    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.hinter.hint(line, pos, ctx)
    }
}

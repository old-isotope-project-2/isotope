/*!
Build `isotope` terms from an AST
*/
use crate::ast::{self, Let, Scope, Statement, Binding};
use crate::parser::*;
use thiserror::Error;
use elysees::Arc;
use smallvec::{smallvec, SmallVec};

use isotope::prelude::*;
use std::hash::BuildHasher;
use std::collections::HashMap;

/// Builds `isotope` terms from an AST
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct Builder<S: BuildHasher = ahash::RandomState> {
    //TODO: optimize this...
    symbols: HashMap<String, Symbol, S>,
    symbol_stack: Vec<HashMap<String, Symbol, S>>,
    param_ty_stack: Vec<TermId>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Symbol {
    /// The depth this symbol was defined at
    depth: u64,
    /// The symbol's value
    value: TermId,
}

/// An error building an `isotope` term
#[derive(Error, Debug, Clone, Eq, PartialEq, Hash)]
pub enum BuildError {
    /// Cannot infer the value of a hole
    #[error("Cannot infer the value of a hole")]
    CannotInfer,
    /// A parse error
    #[error("Error parsing {0}")]
    ParseError(String),
    /// A type mismatch
    #[error("Type mismatch: {:#?}, {:#?}", 0, 1)]
    TypeMismatch(TermId, TermId),
    /// An undefined symbol
    #[error("Undefined symbol {0}")]
    UndefinedSymbol(String),
    /// An error constructing an isotope value
    #[error("isotope error: {0}")]
    IsotopeError(isotope::Error),
}

impl From<isotope::Error> for BuildError {
    fn from(err: isotope::Error) -> BuildError {
        BuildError::IsotopeError(err)
    }
}

impl<S: BuildHasher + Clone + Default> Builder<S> {
    /// Parse and build a term
    pub fn parse_term<'a>(&mut self, input: &'a str) -> Result<(&'a str, TermId), BuildError> {
        self.parse_term_full(input)
            .map(|(rest, (_, term))| (rest, term))
    }
    /// Parse and build a term, returning the term and the AST
    pub fn parse_term_full<'a>(
        &mut self,
        input: &'a str,
    ) -> Result<(&'a str, (ast::Term, TermId)), BuildError> {
        let (rest, term) = term(input).map_err(|_| BuildError::ParseError(input.to_owned()))?;
        let value = self.term(&term)?;
        Ok((rest, (term, value)))
    }
    /// Build a term
    pub fn term(&mut self, term: &ast::Term) -> Result<TermId, BuildError> {
        use ast::Term::*;
        let result = match term {
            Ident(i) => self.ident(i)?,
            Hole => return Err(BuildError::CannotInfer),
            Var(v) => self.var(v)?.into_id(),
            Sexpr(s) => self.sexpr(s)?,
            Lambda(l) => self.lambda(l)?.into_id(),
            Pi(p) => self.pi(p)?.into_id(),
            Universe(u) => u.into_id(),
            Finite(f) => f.into_id(),
            Ix(i) => i.into_id(),
            Nat => isotope::prelude::Nat.into_id(),
            Natural(n) => n.clone().into_id(),
            Scope(s) => self.scope(s)?,
        };
        Ok(result)
    }
    /// Build an ident
    pub fn ident(&mut self, ident: &str) -> Result<TermId, BuildError> {
        let Symbol { depth, value } = self
            .symbols
            .get(ident)
            .ok_or_else(|| BuildError::UndefinedSymbol(ident.to_owned()))?;
        assert!(
            *depth <= self.param_depth(), 
            "Variable bindings should never be pulled out of a function scope, but have depth={} > param_depth={}", 
            depth, 
            self.param_depth()
        );
        let shift = self.param_depth() - *depth;
        let result = value.shift(shift as i64, 0)?.unwrap_or_else(|| value.clone());
        if shift == 0 {
            debug_assert_eq!(*value, result);
        }
        Ok(result)
    }
    /// Build a variable
    pub fn var(&mut self, var: &ast::Var) -> Result<Var, BuildError> {
        let ty = if let Some(ty) = &var.ty {
            let ty = self.term(ty)?;
            if let Some(infer_ty) = self.ix_ty(var.ix) {
                if ty != *infer_ty {
                    return Err(BuildError::TypeMismatch(ty, infer_ty.clone()))
                }
            }
            ty
        } else if let Some(ty) = self.ix_ty(var.ix) {
            ty.clone()
        } else {
            return Err(BuildError::CannotInfer)
        };
        let var = Var::try_new(var.ix, ty)?;
        Ok(var)
    }
    /// Get a variable type at a given index
    fn ix_ty(&self, ix: u64) -> Option<&TermId> {
        self.param_depth().checked_sub(ix + 1).map(|ix| &self.param_ty_stack[ix as usize])
    }
    /// Build an S-expression
    #[inline]
    pub fn sexpr(&mut self, sexpr: &ast::Sexpr) -> Result<TermId, BuildError> {
        self.sexpr_slice(&sexpr.0[..])
    }
    /// Build an S-expression from a slice
    #[inline]
    pub fn sexpr_slice(&mut self, args: &[Arc<ast::Term>]) -> Result<TermId, BuildError> {
        if args.len() == 0 {
            todo!("Unit type")
        }
        let mut f = self.term(&args[0])?;
        for arg in &args[1..] {
            let arg = self.term(arg)?;
            f = App::try_new(f, arg)?.into_id()
        }
        Ok(f)
    }
    /// Build a lambda function
    pub fn lambda(&mut self, lambda: &ast::Parametrized) -> Result<Lambda, BuildError> {
        // Let's not support inference for lambda arguments quite yet...
        let arg_ty = lambda.parameter.ty.as_ref().ok_or(BuildError::CannotInfer)?;
        let arg_ty = self.term(arg_ty)?;
        self.push(smallvec![arg_ty.clone()]);
        let result = self.lambda_inner(lambda, arg_ty);
        self.pop(1);
        result
    }
    fn lambda_inner(&mut self, lambda: &ast::Parametrized, arg_ty: TermId) -> Result<Lambda, BuildError> {
        let var = Var::try_new(0, arg_ty)?;
        let arg_ty = var.ty().into_id(); // Avoid unnecessary clone of `arg_ty` if `arg_ty` is not actually a type
        if let Some(name) = &lambda.parameter.name {
            // Avoid re-parsing the type of the lambda argument for type-checking purposes
            self.bind_name(name.clone(), var.into_id())?;
        }
        let term = self.term(&lambda.term)?;
        let result = Lambda::try_new(arg_ty, term)?;
        Ok(result)
    }    
    /// Build a pi type
    pub fn pi(&mut self, pi: &ast::Parametrized) -> Result<Pi, BuildError> {
        // Let's not support inference for lambda arguments quite yet...
        let arg_ty = pi.parameter.ty.as_ref().ok_or(BuildError::CannotInfer)?;
        let arg_ty = self.term(arg_ty)?;
        self.push(smallvec![arg_ty.clone()]);
        let result = self.pi_inner(pi, arg_ty);
        self.pop(1);
        result
    }
    fn pi_inner(&mut self, pi: &ast::Parametrized, arg_ty: TermId) -> Result<Pi, BuildError> {
        let var = Var::try_new(0, arg_ty)?;
        let arg_ty = var.ty().into_id(); // Avoid unnecessary clone of `arg_ty` if `arg_ty` is not actually a type
        if let Some(name) = &pi.parameter.name {
            // Avoid re-parsing the type of the lambda argument for type-checking purposes
            self.bind_name(name.clone(), var.into_id())?;
        }
        let term = self.term(&pi.term)?;
        let result = Pi::try_new(arg_ty, term)?;
        Ok(result)
    }
    /// Build a scope
    pub fn scope(&mut self, scope: &Scope) -> Result<TermId, BuildError> {
        self.push(smallvec![]);
        let result = self.scope_inner(scope);
        self.pop(0);
        result
    }
    fn scope_inner(&mut self, scope: &Scope) -> Result<TermId, BuildError> {
        for stmt in &scope.statements {
            self.stmt(stmt)?
        }
        self.term(&scope.retv)
    }
    /// Get the parameter depth
    fn param_depth(&self) -> u64 {
        self.param_ty_stack.len() as u64
    }
    /// Push a scope
    fn push(&mut self, params: SmallVec<[TermId; 2]>) {
        self.param_ty_stack.extend(params);
        self.symbol_stack.push(self.symbols.clone());
    }
    /// Pop a scope
    fn pop(&mut self, params: usize) {
        for _ in 0..params {
            self.param_ty_stack.pop();
        }
        self.symbols = self.symbol_stack.pop().unwrap_or_default();
    }
    /// Handle a statement
    pub fn stmt(&mut self, stmt: &Statement) -> Result<(), BuildError> {
        match stmt {
            Statement::Let(l) => self.let_stmt(l),
        }
    }
    /// Handle a let statement
    pub fn let_stmt(&mut self, let_stmt: &Let) -> Result<(), BuildError> {
        let Let { binding, value } = let_stmt;
        let value = self.term(value)?;
        self.set_binding(binding, value)?;
        Ok(())
    }
    /// Set a binding to a value, returning the type of the binding
    pub fn set_binding(&mut self, binding: &Binding, value: TermId) -> Result<Option<TermId>, BuildError> {
        let ty = if let Some(ty) = &binding.ty {
            let ty = self.term(ty)?;
            if value.ty() != ty {
                return Err(BuildError::TypeMismatch(value.ty().into_id(), ty))
            }
            Some(ty)
        } else {
            None
        };
        if let Some(name) = &binding.name {
            self.bind_name(name.clone(), value)?;
        }
        Ok(ty)
    }
    /// Bind a name
    pub fn bind_name(&mut self, name: String, value: TermId) -> Result<(), BuildError> {
        self.symbols.insert(
            name.clone(),
            Symbol {
                depth: self.param_depth(),
                value,
            },
        );
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn trivial_scope_substitution() {
        let mut builder: Builder = Builder::default();
        let test_strings = [
            ("A", "#set"),
            ("long_name_with_underscores", "#finite[35]"),
            ("index", "35[34]"),
            ("f", "#lambda x: #nat => x"),
            ("f", "#lambda _: #nat => #0"),
            ("unary", "#pi _: #nat => #nat"),
            ("x", "#0[#nat]"),
            ("nested_scope", "{ #let x = 57; x }"),
            ("sexpr", "((#lambda x: #nat => x) 53)")
        ];
        for &(name, value) in &test_strings {
            let binding = format!(
                "{{
                    #let {0} = {1};
                    {0}
                }}",
                name, value
            );
            assert_eq!(
                builder.parse_term(&binding).expect(value),
                builder.parse_term(value).expect(value)
            );
            assert_eq!(
                builder.parse_term(name),
                Err(BuildError::UndefinedSymbol(name.to_owned()))
            );
        }
    }
    #[test]
    fn type_checking() {
        let mut builder: Builder = Builder::default();
        let positive_examples = [
            "#let _: #nat = 56;",
            "#let _: #finite[53] = 53[1];"
        ];
        for &example in &positive_examples {
            let (rest, s) = stmt(example).unwrap();
            assert_eq!(rest, "");
            builder.stmt(&s).expect("Positive statement failed to handle");
        }
        let negative_examples = [
            "#let _: #nat = 53[1];",
            "#let _: #finite[53] = 56;"
        ];
        for &example in &negative_examples {
            let (rest, s) = stmt(example).unwrap();
            assert_eq!(rest, "");
            builder.stmt(&s).expect_err("Negative statement failed to error");
        }
    }
}

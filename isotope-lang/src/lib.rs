/*!
# `isotope-lang`

`isotope-lang` is a simple textual representation of the `isotope` IR, designed primarily to facilitate debugging and as a simple proof-of-concept language. A few convenience features, e.g. named bindings, are included as syntax sugar over the raw IR.
*/
#![forbid(missing_debug_implementations, missing_docs)]
#![warn(clippy::all)]

pub mod ast;
pub mod builder;
pub mod parser;

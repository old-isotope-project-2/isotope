/*!
AST for `isotope`
*/
use elysees::Arc;
use isotope::prelude::{Finite, Ix, Universe};
use num::BigUint;

/// A term in the dependent type theory underlying `isotope`
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Term {
    // Identifiers and placeholders
    /// An identifier
    Ident(String),
    /// A hole, the value of which is to be inferred
    Hole,
    /// A de-Bruijn index
    Var(Var),

    // Basic language elements
    /// An S-expression
    Sexpr(Sexpr),
    /// A lambda fuction
    Lambda(Parametrized),
    /// A pi type
    Pi(Parametrized),

    // Primitive types and values
    /// A typing universe
    Universe(Universe),
    /// A finite type
    Finite(Finite),
    /// An index into a finite type
    Ix(Ix),
    /// The of natural number
    Nat,
    /// A natural number
    Natural(BigUint),

    // Syntax sugar
    /// A scope
    Scope(Scope),
}

impl Term {
    /// Parse an ident into a term, which may be a hole
    #[inline]
    pub fn ident(ident: &str) -> Term {
        if ident == "_" {
            Term::Hole
        } else {
            Term::Ident(ident.to_owned())
        }
    }
    /// Parse an ident into a string or a hole
    #[inline]
    pub fn ident_str(ident: &str) -> Option<String> {
        if ident == "_" {
            None
        } else {
            Some(ident.to_owned())
        }
    }
}

/// A de-Bruijn index
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Var {
    /// The index
    pub ix: u64,
    /// The type of the index, if specified
    pub ty: Option<Arc<Term>>,
}

/// A binding
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Binding {
    /// The name of the binding
    pub name: Option<String>,
    /// The type of the binding,
    pub ty: Option<Arc<Term>>,
}

/// A parametrized term
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Parametrized {
    /// The parameter of this lambda
    pub parameter: Binding,
    /// The term being parametrized
    pub term: Arc<Term>,
}

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Sexpr(pub Vec<Arc<Term>>);

/// A scope
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Scope {
    /// The statements in this scope
    pub statements: Vec<Statement>,
    /// The return value of this scope
    pub retv: Arc<Term>,
}

/// A statement
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Statement {
    /// A let statement
    Let(Let),
}

/// A let-statement
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Let {
    /// The binding being defined
    pub binding: Binding,
    /// The value given to the binding
    pub value: Arc<Term>,
}

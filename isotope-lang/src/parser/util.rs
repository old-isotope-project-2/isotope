/*!
Utilities and fragments for implementing the main parser
*/

use super::*;

/// Parse whitespace between `isotope` identifiers
///
/// This includes both actual whitespace and comments
///
/// # Example
/// ```rust
/// # use isotope_lang::parser::ws;
/// assert_eq!(
///     ws("  /* multiline // comment */ // line comment \ndone").unwrap().0,
///     "done"
/// );
/// ```
pub fn ws(input: &str) -> IResult<&str, &str> {
    recognize(many1_count(alt((
        multispace1,
        line_comment,
        multiline_comment,
    ))))(input)
}

/// Parse a single line comment
///
/// Single-line comments begin with `//` and run to the end of the line, e.g.
/// ```text
/// // I'm a single line comment
/// ```
///
/// # Example
/// ```rust
/// # use isotope_lang::parser::line_comment;
/// assert_eq!(
///     line_comment("//line comment \n//hello"),
///     Ok(("\n//hello", "line comment "))
/// );
/// assert_eq!(
///     line_comment("// // nested\nhello"),
///     Ok(("\nhello", " // nested"))
/// )
/// ```
pub fn line_comment(input: &str) -> IResult<&str, &str> {
    preceded(tag("//"), not_line_ending)(input)
}

/// Parse a multi-line comment.
///
/// Multi-line comments are delimited by `/*` and `*/`, and may be nested, e.g.
/// ```text
/// /*
/// I'm a comment!
///     /*
///     I'm a nested comment!
///     */
/// */
/// ```
///
/// # Example
/// ```rust
/// # use isotope_lang::parser::multiline_comment;
/// assert_eq!(
///     multiline_comment("/* comment */hello"),
///     Ok(("hello", " comment "))
/// );
/// assert_eq!(
///     multiline_comment("/*/*nested*/*//*other*/"),
///     Ok(("/*other*/", "/*nested*/"))
/// );
/// assert_eq!(
///     multiline_comment(
///         "/* deeply /*/* nested */ and */ se /* parated */ */"
///     ),
///     Ok(("", " deeply /*/* nested */ and */ se /* parated */ "))
/// );
/// ```
pub fn multiline_comment(input: &str) -> IResult<&str, &str> {
    fn multiline_inner(input: &str) -> IResult<&str, ()> {
        let mut chars = input.chars();
        let mut rest = input;
        while let Some(c) = chars.next() {
            if c == '*' {
                if let Some('/') = chars.next() {
                    break;
                }
            }
            if c == '/' {
                if let Some('*') = chars.next() {
                    let (after_comment, _) = multiline_comment(rest)?;
                    chars = after_comment.chars();
                }
            }
            rest = chars.as_str();
        }
        Ok((rest, ()))
    }
    delimited(tag("/*"), recognize(multiline_inner), tag("*/"))(input)
}

/// Parse a decimal integer literal as a u64.
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::u64_dec;
/// assert_eq!(u64_dec("5"), Ok(("", 5)));
/// // Does not recognize hexadecimal literals, etc!
/// assert_eq!(u64_dec("0x5"), Ok(("x5", 0)));
/// // Overflow!
/// assert!(u64_dec("100000000000000000000").is_err());
/// ```
pub fn u64_dec(input: &str) -> IResult<&str, u64> {
    map_res(digit1, str::parse)(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn line_comment_works() {
        let comment_examples = [
            "// I'm a line comment",
            "// I'm a line comment, with /* a non-nested comment */",
            "// /* I'm a // line comment with a /* nested */ multiline (and line) comment */",
        ];
        for s in comment_examples.iter().copied() {
            assert_eq!(line_comment(s).unwrap().0, "")
        }
    }

    #[test]
    fn multiline_comment_works() {
        let comment_examples = [
            "/* Hello, I'm a /* nested /* comment */ */ */",
            "/* I'm a non-nested comment */",
            "/* I'm a // line comment in a /* nested */ multiline comment */",
        ];
        for s in comment_examples.iter().copied() {
            assert_eq!(multiline_comment(s).unwrap().0, "")
        }
    }

    #[test]
    fn u64_dec_works() {
        let full_examples = [("53", 53), ("456", 456), ("352", 352)];
        for (s, n) in full_examples.iter().copied() {
            assert_eq!(u64_dec(s), Ok(("", n)))
        }
    }
}

/*!
A parser for `isotope` terms
*/

//use ast::*;
use elysees::Arc;
use isotope::prelude::{Finite, Ix, Universe};
use nom::branch::*;
use nom::bytes::complete::*;
use nom::character::{complete::*, *};
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use num::BigUint;

mod util;
pub use util::*;

use crate::ast::*;

/// The set of special characters
pub const SPECIAL_CHARACTERS: &str = "\n\r\t :[](){}#\"\\,;.=";

/// The keyword for a finite type
pub const FINITE: &str = "#finite";
/// The keyword for a typing universe
pub const UNIVERSE: &str = "#universe";
/// The keyword for the universe of sets
pub const SET: &str = "#set";
/// The keyword for the universe of simple types
pub const TYPE: &str = "#type";
/// The keyword for a lambda function
pub const LAMBDA: &str = "#lambda";
/// The keyword for a pi type
pub const PI: &str = "#pi";
/// The keyword for a let statement
pub const LET: &str = "#let";
/// The type of natural numbers
pub const NAT: &str = "#nat";

/// Parse an `isotope` term
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::term;
/// # use isotope_lang::ast::Term;
/// # use isotope_lang::ast::Term::*;
/// # use isotope::{prelude::{Finite, Universe}};
/// // Identifiers
/// assert_eq!(term("x").unwrap(), ("", Ident("x".to_owned())));
///
/// // Basic constants
/// assert_eq!(term("#finite[8848]").unwrap(), ("", Term::Finite(Finite(8848))));
/// assert_eq!(term("#universe[1]").unwrap(), ("", Term::Universe(Universe::new(1))));
/// assert_eq!(term("0xABC").unwrap(), ("", Term::Natural(0xABCu32.into())))
/// ```
pub fn term(input: &str) -> IResult<&str, Term> {
    alt((
        // Identifiers and placeholders
        map(ident, Term::ident),
        map(var, Term::Var),
        // Basic expressions
        map(sexpr, Term::Sexpr),
        map(lambda, Term::Lambda),
        map(pi, Term::Pi),
        // Primitive types and values
        map(universe, Term::Universe),
        map(finite, Term::Finite),
        map(ix, Term::Ix),
        map(tag(NAT), |_| Term::Nat),
        map(natural, Term::Natural),
        // Syntax sugar
        map(scope, Term::Scope),
    ))(input)
}

/// Parse a string forming a valid `isotope` identifier
///
/// An `isotope` identifier may be any sequence of non-whitespace characters which does not
/// contain a special character. This parser does *not* consume preceding whitespace!
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::ident;
/// assert_eq!(ident("hello "), Ok((" ", "hello")));
/// assert!(ident(" bye").is_err());
/// assert!(ident("0x35").is_err());
/// assert_eq!(ident("x35"), Ok(("", "x35")));
/// assert_eq!(ident("你好"), Ok(("", "你好")));
/// let arabic = ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| {
        !is_digit(ident.as_bytes()[0])
    })(input)
}

/// Parse a de-Bruijn index
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::*;
/// assert!(var("#1").is_ok());
/// assert!(var("#0[#nat]").is_ok());
/// ```
pub fn var(input: &str) -> IResult<&str, Var> {
    map(
        pair(
            preceded(tag("#"), u64_dec),
            opt(delimited(
                preceded(tag("["), opt(ws)),
                term,
                preceded(opt(ws), tag("]")),
            )),
        ),
        |(ix, ty)| Var {
            ix,
            ty: ty.map(Arc::new),
        },
    )(input)
}

/// Parse an S-expression, i.e. repeated left-associative application
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::*;
/// assert!(sexpr("(f x)").is_ok());
/// ```
pub fn sexpr(input: &str) -> IResult<&str, Sexpr> {
    delimited(
        preceded(tag("("), opt(ws)),
        map(many0(map(terminated(term, opt(ws)), Arc::new)), Sexpr),
        tag(")"),
    )(input)
}

/// Parse a lambda function
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::*;
/// assert!(lambda("#lambda x: A => x").is_ok());
/// ```
pub fn lambda(input: &str) -> IResult<&str, Parametrized> {
    preceded(preceded(tag(LAMBDA), ws), parametrized)(input)
}

/// Parse a pi type
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::*;
/// assert!(pi("#pi x: A => B").is_ok());
/// ```
pub fn pi(input: &str) -> IResult<&str, Parametrized> {
    preceded(preceded(tag(PI), ws), parametrized)(input)
}

/// Parse a parametrized term
pub fn parametrized(input: &str) -> IResult<&str, Parametrized> {
    map(
        separated_pair(binding, delimited(opt(ws), tag("=>"), opt(ws)), term),
        |(binding, term)| Parametrized {
            parameter: binding,
            term: term.into(),
        },
    )(input)
}

/// Parse a variable binding
///
/// # Examples
/// ```rust
/// # use isotope_lang::{parser::binding, ast::{Binding, Term}};
/// let x_A = Binding {
///     name: Some("x".to_owned()),
///     ty: Some(Term::ident("y").into())
/// };
/// assert_eq!(binding("x : y").unwrap().1, x_A);
/// assert_eq!(binding("x: y").unwrap().1, x_A);
/// assert_eq!(binding("x :y").unwrap().1, x_A);
/// assert_eq!(binding("x:y").unwrap().1, x_A);
/// ```
pub fn binding(input: &str) -> IResult<&str, Binding> {
    map(
        tuple((
            ident,
            opt(preceded(delimited(opt(ws), tag(":"), opt(ws)), term)),
        )),
        |(ident, ty)| Binding {
            name: Term::ident_str(ident),
            ty: ty.map(Arc::new),
        },
    )(input)
}

/// Parse a finite type
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::finite;
/// # use isotope::prelude::Finite;
/// assert_eq!(finite("#finite[32]"), Ok(("", Finite(32))));
/// assert_eq!(finite("#finite[ 64 ]"), Ok(("", Finite(64))));
/// ```
pub fn finite(input: &str) -> IResult<&str, Finite> {
    preceded(
        tag(FINITE),
        delimited(
            preceded(tag("["), opt(ws)),
            map(u64_dec, Finite),
            preceded(opt(ws), tag("]")),
        ),
    )(input)
}

/// Parse an index into a finite type
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::ix;
/// # use isotope::prelude::{Finite, Ix};
/// assert_eq!(ix("435[32]"), Ok(("", Ix::try_new(32, Finite(435)).unwrap())));
/// assert_eq!(ix("134[64]"), Ok(("", Ix::try_new(64, Finite(134)).unwrap())));
/// assert!(ix("2[2]").is_err());
/// ```
pub fn ix(input: &str) -> IResult<&str, Ix> {
    map_res(
        pair(
            u64_dec,
            delimited(
                preceded(tag("["), opt(ws)),
                u64_dec,
                preceded(opt(ws), tag("]")),
            ),
        ),
        |(ty, ix)| Ix::try_new(ix, Finite(ty)),
    )(input)
}

/// Parse a typing universe
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::universe;
/// # use isotope::prelude::Universe;
/// assert_eq!(universe("#universe[32]"), Ok(("", Universe::new(32))));
/// assert_eq!(universe("#universe[  64 ]"), Ok(("", Universe::new(64))));
/// assert_eq!(universe("#set"), Ok(("", Universe::SET)));
/// assert_eq!(universe("#type"), Ok(("", Universe::TYPE)));
/// ```
pub fn universe(input: &str) -> IResult<&str, Universe> {
    alt((
        preceded(
            tag(UNIVERSE),
            delimited(
                preceded(tag("["), opt(ws)),
                map(u64_dec, Universe::new),
                preceded(opt(ws), tag("]")),
            ),
        ),
        map(tag(SET), |_| Universe::SET),
        map(tag(TYPE), |_| Universe::TYPE),
    ))(input)
}

/// A scope, terminated by a value
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::scope;
/// # use isotope_lang::ast::*;
/// assert!(scope("{ #let x = #set; x }").is_ok());
/// ```
pub fn scope(input: &str) -> IResult<&str, Scope> {
    map(
        delimited(
            preceded(tag("{"), opt(ws)),
            pair(many0(terminated(stmt, opt(ws))), terminated(term, opt(ws))),
            tag("}"),
        ),
        |(statements, retv)| Scope {
            statements,
            retv: Arc::new(retv),
        },
    )(input)
}

/// A statement
pub fn stmt(input: &str) -> IResult<&str, Statement> {
    map(let_stmt, Statement::Let)(input)
}

/// Parse a natural number literal
///
/// A natural number literal is either
/// - A sequence of decimal digits, e.g. `00120013`
/// - A sequence of hexadecimal digits prefixed by `0x`, e.g. `0xABC`
/// - A sequence of octal digits prefixed by `0o`, e.g. `0o163`
/// - A sequence of binary digits prefixed by `0b`, e.g. `0b1101`
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::natural;
/// assert_eq!(natural("0123hello"), Ok(("hello", 123u32.into())));
/// assert_eq!(natural("0xABCH"), Ok(("H", 0xABCu32.into())));
/// assert_eq!(natural("0o129"), Ok(("9", 0o12u32.into())));
/// assert_eq!(natural("0b0111012"), Ok(("2", 0b011101u32.into())));
/// assert_eq!(natural("0b2"), Ok(("b2", 0u32.into())));
/// ```
pub fn natural(input: &str) -> IResult<&str, BigUint> {
    alt((
        map_opt(preceded(tag("0x"), hex_digit1), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 16)
        }),
        map_opt(preceded(tag("0o"), oct_digit1), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 8)
        }),
        map_opt(preceded(tag("0b"), is_a("01")), |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 2)
        }),
        map_opt(digit1, |digits: &str| {
            BigUint::parse_bytes(digits.as_bytes(), 10)
        }),
    ))(input)
}

/// Parse a let statement
///
/// # Examples
/// ```rust
/// # use isotope_lang::parser::let_stmt;
/// # use isotope_lang::ast::*;
/// assert!(let_stmt("#let x = #set;").is_ok());
/// ```
pub fn let_stmt(input: &str) -> IResult<&str, Let> {
    map(
        delimited(
            preceded(tag(LET), ws),
            separated_pair(binding, delimited(opt(ws), tag("="), opt(ws)), term),
            preceded(opt(ws), tag(";")),
        ),
        |(binding, term)| Let {
            binding,
            value: Arc::new(term),
        },
    )(input)
}

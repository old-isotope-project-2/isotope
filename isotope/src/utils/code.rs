/*!
Hash-code generation
*/

/// An object which may be assigned a unique hash-code
pub trait Code {
    /// Get the code of this object
    fn code(&self) -> u64;
}

impl Code for u64 {
    #[inline]
    fn code(&self) -> u64 {
        *self
    }
}

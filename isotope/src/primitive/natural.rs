/*!
Natural number
*/
use super::*;
use crate::error::*;
use crate::prelude::*;
use num::BigUint;

/// A natural number type
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Nat;

impl Value for Nat {
    #[inline]
    fn into_term(self) -> Term {
        Term::Nat(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        SET.into()
    }
    #[inline]
    fn tyck(&self) -> bool {
        true
    }
    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        true
    }
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        NORMAL_TERM
    }
    #[inline]
    fn fvb(&self) -> u64 {
        0
    }
    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
}

impl Code for Nat {
    fn code(&self) -> u64 {
        let mut hasher: AHasher = AHasher::new_with_keys(88, 48);
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl PartialEq<Term> for Nat {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Nat(n) => self.eq(n),
            _ => false,
        }
    }
}

/// A natural number
pub type Natural = BigUint;

impl Value for Natural {
    #[inline]
    fn into_term(self) -> Term {
        Term::Natural(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        Term::Nat(Nat).into_term().into()
    }
    #[inline]
    fn tyck(&self) -> bool {
        true
    }
    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        true
    }
    #[inline]
    fn is_ty(&self) -> bool {
        false
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        NORMAL_TERM
    }
    #[inline]
    fn fvb(&self) -> u64 {
        0
    }
    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    #[inline]
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
}

impl Code for Natural {
    fn code(&self) -> u64 {
        let mut hasher: AHasher = AHasher::new_with_keys(8848, 8849);
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl PartialEq<Term> for Natural {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Natural(n) => self.eq(n),
            _ => false,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_nat_properties() {
        let n = BigUint::from(4u64);
        assert_eq!(Nat, *n.ty());
        assert_eq!(n.fvb(), 0);
        assert!(!n.is_ty());
        assert!(n.tyck());
        assert_eq!(SET, *Nat.ty());
        assert_eq!(Nat.fvb(), 0);
        assert!(Nat.is_ty());
        assert!(Nat.tyck());
    }
}

/*!
Types containing a finite number of values, and elements of such types
*/
use super::*;
use crate::error::*;
use crate::prelude::*;

/// A finite type with `n` values
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Finite(pub u64);

impl Finite {
    /// Try to index into a finite type. Return an error if the index is out of bounds.
    #[inline]
    pub fn try_ix(self, ix: u64) -> Result<Ix, ()> {
        Ix::try_new(ix, self)
    }
    /// Index into a finite type. Panic if the index is out of bounds
    #[inline]
    pub fn ix(self, ix: u64) -> Ix {
        Ix::try_new(ix, self).unwrap()
    }
}

impl Value for Finite {
    #[inline]
    fn into_term(self) -> Term {
        Term::Finite(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        SET.into()
    }
    #[inline]
    fn tyck(&self) -> bool {
        true
    }
    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        true
    }
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        NORMAL_TERM
    }
    #[inline]
    fn fvb(&self) -> u64 {
        0
    }
    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    #[inline]
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
}

impl PartialEq<Term> for Finite {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Finite(f) => self.eq(f),
            _ => false,
        }
    }
}

impl Code for Finite {
    fn code(&self) -> u64 {
        let mut hasher = AHasher::new_with_keys(352, 13432);
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl Repr for Finite {
    #[inline]
    fn denote(&self) -> CTerm {
        CTerm::Term(Term::Finite(*self))
    }
}

/// An "index into," i.e. an element of, a finite type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Ix {
    /// The index into the type
    ix: u64,
    /// The finite type this is an index into
    ty: Finite,
}

impl Ix {
    /// Try to create a new index into a finite type. Return an error if the index is out of bounds
    #[inline]
    pub fn try_new(ix: u64, ty: Finite) -> Result<Ix, ()> {
        if ix >= ty.0 {
            Err(())
        } else {
            Ok(Ix { ix, ty })
        }
    }
}

impl Value for Ix {
    #[inline]
    fn into_term(self) -> Term {
        Term::Ix(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        self.ty.into_term().into()
    }
    #[inline]
    fn tyck(&self) -> bool {
        true
    }
    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        true
    }
    #[inline]
    fn is_ty(&self) -> bool {
        false
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        NORMAL_TERM
    }
    #[inline]
    fn fvb(&self) -> u64 {
        0
    }
    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    #[inline]
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    #[inline]    
    fn ind_ty(&self, _family: &TermId, _branches: &[TermId]) -> Result<TermId, Error> {
        todo!()
    }
}

impl PartialEq<Term> for Ix {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Ix(ix) => self.eq(ix),
            _ => false,
        }
    }
}

impl Code for Ix {
    fn code(&self) -> u64 {
        let mut hasher = AHasher::new_with_keys(452, 596);
        self.hash(&mut hasher);
        hasher.finish()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    /// Basic indexing of finite types
    #[test]
    fn basic_finite_indexing() {
        assert_eq!(
            Ix::try_new(34, Finite(83)).unwrap(),
            Finite(83).try_ix(34).unwrap()
        );
        assert_ne!(
            Ix::try_new(34, Finite(83)).unwrap(),
            Ix::try_new(34, Finite(82)).unwrap()
        );
        assert_ne!(
            Ix::try_new(34, Finite(83)).unwrap(),
            Ix::try_new(33, Finite(83)).unwrap()
        );
        assert_eq!(Ix::try_new(34, Finite(34)), Err(()));

        assert_eq!(Finite(56), *Ix::try_new(52, Finite(56)).unwrap().ty());
        assert_eq!(SET, Finite(56).ty())
    }

    #[test]
    fn finite_denote() {
        assert_eq!(Finite(56), *Finite(56).denote());
    }
}

/*!
Primitive `isotope` constants

Includes natural numbers, finite types, and small bitvectors
*/

pub mod array;
pub mod bits;
pub mod finite;
pub mod natural;

use ahash::AHasher;
use std::hash::{Hash, Hasher};

/*!
Error handling
*/
use thiserror::Error;

/// A generic `isotope` error
#[derive(Error, Debug, Clone, Eq, PartialEq, Hash)]
pub enum Error {
    /// The given value is not a type
    #[error("The given value is not a type")]
    NotAType,
    /// A type mismatch
    #[error("There has been a type mismatch")]
    TypeMismatch,
    /// Failed type check
    #[error("Failed type check")]
    TyckFailure,
    /// The given value is not a function
    #[error("The given value is not a function")]
    NotAFunction,
    /// The given value is not a valid family for path induction
    #[error("The given value is not a valid family for path induction")]
    NotAFamily,
    /// The given type is not inductive
    #[error("The given type is not inductive")]
    NotInductive,
    /// A type cannot directly depend on itself
    #[error("A type cannot directly depend on itself")]
    CyclicType,
    /// A shift went out of bounds
    #[error("A shift went out-of-bounds")]
    ShiftOutOfBounds,
    /// An inconsistently set flag
    #[error("An inconsistently set flag")]
    InconsistentFlag,
}

impl Error {
    /// A checked shift
    pub fn checked_shift(ix: u64, shift: i64, base: u64) -> Result<u64, Error> {
        if shift < 0 {
            ix.checked_sub(-shift as u64)
                .filter(|ix| *ix >= base)
                .ok_or(Error::ShiftOutOfBounds)
        } else {
            ix.checked_add(shift as u64).ok_or(Error::ShiftOutOfBounds)
        }
    }
}

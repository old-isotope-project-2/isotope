/*!
Commonly used items
*/

pub use crate::for_term;
pub use crate::primitive::{
    finite::{Finite, Ix},
    natural::{Nat, Natural},
};
pub use crate::term::eval::EvalCtx;
pub use crate::term::{
    app::App,
    flags::{AtomicFlags, Flags, NORMAL_TERM},
    lambda::Lambda,
    pi::Pi,
    tyck::TyCtx,
    universe::{Universe, SET, TYPE},
    var::Var,
    CTerm, Term, TermId, Value,
};
pub use crate::repr::{Repr, Type};
pub use crate::utils::code::Code;

/*!
# `isotope`

`isotope` is a prototype intermediate representation (IR) aiming to support dependently typed verification of Rust-like programs. To do so, we equip an [RVSDG](https://arxiv.org/abs/1912.05036)-like IR with a system of [linear dependent types](https://www.cl.cam.ac.uk/~nk480/dlnl-paper.pdf) extended with a notion of [borrowing](https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html).

This crate is a very work in progress Rust implementation of the main data structures and algorithms of the `isotope` IR, including internal representations, the type checker, the borrow checker, and a simple expression evaluator.

Questions and contributions are highly appreciated: please open an issue or pull request, or e-mail the author at <jad.ghalayini@cs.ox.ac.uk>.
*/
#![forbid(missing_debug_implementations, missing_docs)]
#![warn(clippy::all)]

pub mod error;
pub mod prelude;
pub mod prettyprinter;
pub mod primitive;
pub mod term;
pub mod repr;
pub mod utils;

pub use error::Error;

/*!
A prettyprinter for `isotope` terms and programs
*/
use std::fmt::{self, Formatter};

/// A type which can be prettyprinted
pub trait Prettyprint<P> {
    /// Prettyprint this object with a given printer
    fn prettyprint(&self, printer: &mut P, fmt: &mut Formatter) -> fmt::Result;
}

/*!
Terms in the dependent type theory underlying `isotope`
*/
use crate::error::*;
use crate::prelude::*;
use ahash::AHasher;
use either::Either;
use elysees::Arc;
use std::fmt::{self, Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::Deref;

pub mod app;
pub mod data;
pub mod flags;
pub mod ind;
pub mod lambda;
pub mod path;
pub mod pi;
pub mod eval;
pub mod tyck;
pub mod universe;
pub mod var;

use app::*;
use flags::*;
use ind::*;
use lambda::*;
use path::*;
use pi::*;
use eval::*;
use tyck::*;
use universe::*;
use var::*;

/// A term, wrapped in an `Arc`
#[derive(Clone, Hash, Eq)]
#[repr(transparent)]
pub struct TermId(pub Arc<Term>);

impl TermId {
    /// Create a new `TermId` from a term
    #[inline]
    pub fn new_direct(term: Term) -> TermId {
        TermId(Arc::new(term))
    }
    /// Create a new `TermId` from a term wrapped in an `Arc`
    #[inline]
    pub fn from_arc(term: Arc<Term>) -> TermId {
        TermId(term)
    }
    /// Get the address of this `TermId`
    #[inline]
    pub fn as_ptr(&self) -> *const Term {
        &*self.0
    }
    /// Check two `TermId`s for pointer equality
    #[inline]
    pub fn ptr_eq(&self, other: &TermId) -> bool {
        (&*self.0 as *const _) == (&*other.0 as *const _)
    }
    /// Quickly check two `TermId`s for equality. May yield false positives, but never false negatives
    #[inline]
    pub fn fast_eq(&self, other: &TermId) -> bool {
        self.code() == other.code()
    }
    /// Get this `TermId` as a `Term`
    #[inline]
    pub fn as_term(&self) -> &Term {
        &self.0
    }
    /// Shift this term's free variables above `base` backwards by `n`.
    #[inline]
    pub fn shift_clone(&self, n: i64, base: u64) -> Result<TermId, Error> {
        self.shift(n, base)
            .transpose()
            .unwrap_or_else(|| Ok(self.clone()))
    }
    /// Shift this term's free variables above `base` backwards by `n`.
    #[inline]
    pub fn shift_move(self, n: i64, base: u64) -> Result<TermId, Error> {
        self.shift(n, base).transpose().unwrap_or(Ok(self))
    }
}

impl Debug for TermId {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Debug::fmt(&*self.0, fmt)
    }
}

impl PartialEq for TermId {
    fn eq(&self, other: &TermId) -> bool {
        Arc::ptr_eq(&self.0, &other.0) || &*self.0 == &*other.0
    }
}

impl Deref for TermId {
    type Target = Term;
    #[inline(always)]
    fn deref(&self) -> &Term {
        &*self.0
    }
}

impl Value for TermId {
    #[inline]
    fn into_id(self) -> TermId {
        self
    }
    #[inline]
    fn into_term(self) -> Term {
        (*self.0).clone()
    }
    #[inline]
    fn ty(&self) -> CTerm {
        self.0.ty()
    }
    #[inline]
    fn tyck(&self) -> bool {
        self.0.tyck()
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        self.0.tyck_with(ctx)
    }
    #[inline]
    fn is_ty(&self) -> bool {
        self.0.is_ty()
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        self.0.load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        self.0.fvb()
    }
    #[inline]
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        self.0.shift(n, base)
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        self.0.eval(ctx)
    }
    #[inline]
    fn ind_ty(&self, family: &TermId, branches: &[TermId]) -> Result<TermId, Error> {
        self.0.ind_ty(family, branches)
    }
    #[inline]
    fn ind(
        &self,
        family: &TermId,
        branches: &[TermId],
        params: &[TermId],
        arg: &TermId,
    ) -> Result<Option<TermId>, Error> {
        self.0.ind(family, branches, params, arg)
    }
}

impl Code for TermId {
    #[inline]
    fn code(&self) -> u64 {
        self.0.code()
    }
}

impl PartialEq<Term> for TermId {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        (*self.0).eq(other)
    }
}

/// A term in the dependent type theory underlying `isotope`
#[derive(Clone, Eq, PartialEq)]
pub enum Term {
    /// A variable, given by a de-Bruijn index
    Var(Var),
    /// A function application
    App(App),
    /// A lambda function
    Lambda(Lambda),
    /// A dependent function type
    Pi(Pi),
    /// An inductor
    Ind(Ind),
    /// A typing universe
    Universe(Universe),
    /// An identity type
    Id(Id),
    /// A finite type
    Finite(Finite),
    /// An index into a finite type
    Ix(Ix),
    /// The type of natural number
    Nat(Nat),
    /// A natural number
    Natural(Natural),
}

impl Hash for Term {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.code().hash(hasher)
    }
}

impl Debug for Term {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        for_term!(self; t => Debug::fmt(t, fmt))
    }
}

/// A copy-on-write term
#[derive(Debug, Clone, Eq)]
pub enum CTerm<'a> {
    /// An owned term
    Term(Term),
    /// A borrowed term ID
    Id(&'a TermId),
}

impl Into<CTerm<'static>> for Term {
    #[inline]
    fn into(self) -> CTerm<'static> {
        CTerm::Term(self)
    }
}

impl<'a> Into<CTerm<'a>> for &'a TermId {
    #[inline]
    fn into(self) -> CTerm<'a> {
        CTerm::Id(self)
    }
}

impl Deref for CTerm<'_> {
    type Target = Term;
    fn deref(&self) -> &Term {
        match self {
            CTerm::Term(t) => t,
            CTerm::Id(t) => *t,
        }
    }
}

impl Value for CTerm<'_> {
    #[inline]
    fn into_id(self) -> TermId {
        match self {
            CTerm::Term(term) => term.into_id(),
            CTerm::Id(id) => (*id).clone(),
        }
    }
    #[inline]
    fn into_term(self) -> Term {
        match self {
            CTerm::Term(term) => term,
            CTerm::Id(id) => (**id).clone(),
        }
    }
    #[inline]
    fn ty(&self) -> CTerm {
        (**self).ty()
    }
    #[inline]
    fn tyck(&self) -> bool {
        (**self).tyck()
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        (**self).tyck_with(ctx)
    }
    #[inline]
    fn is_ty(&self) -> bool {
        (**self).is_ty()
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        (**self).load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        (**self).fvb()
    }
    #[inline]
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        (**self).shift(n, base)
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        (**self).eval(ctx)
    }
    #[inline]
    fn ind_ty(&self, family: &TermId, branches: &[TermId]) -> Result<TermId, Error> {
        (**self).ind_ty(family, branches)
    }
    #[inline]
    fn ind(
        &self,
        family: &TermId,
        branches: &[TermId],
        params: &[TermId],
        arg: &TermId,
    ) -> Result<Option<TermId>, Error> {
        (**self).ind(family, branches, params, arg)
    }
}

impl Code for CTerm<'_> {
    #[inline]
    fn code(&self) -> u64 {
        (**self).code()
    }
}

impl<'a, 'b> PartialEq<CTerm<'b>> for CTerm<'a> {
    fn eq(&self, other: &CTerm<'b>) -> bool {
        (**self).eq(&**other)
    }
}

impl PartialEq<TermId> for CTerm<'_> {
    fn eq(&self, other: &TermId) -> bool {
        match self {
            CTerm::Id(id) => (*id).eq(other),
            CTerm::Term(term) => term.eq(&**other),
        }
    }
}

impl PartialEq<CTerm<'_>> for TermId {
    fn eq(&self, other: &CTerm<'_>) -> bool {
        other.eq(self)
    }
}

impl PartialEq<Term> for CTerm<'_> {
    fn eq(&self, other: &Term) -> bool {
        match self {
            CTerm::Id(id) => (*id).eq(other),
            CTerm::Term(term) => term.eq(other),
        }
    }
}

impl PartialEq<CTerm<'_>> for Term {
    fn eq(&self, other: &CTerm<'_>) -> bool {
        other.eq(self)
    }
}

impl Hash for CTerm<'_> {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        (**self).hash(hasher)
    }
}

/// Perform the same computation for every member of the `Term` enum
#[macro_export]
macro_rules! for_term {
    ($v:expr; $e:ident => $t:expr) => {
        match $v {
            Term::Var($e) => $t,
            Term::App($e) => $t,
            Term::Lambda($e) => $t,
            Term::Pi($e) => $t,
            Term::Ind($e) => $t,
            Term::Universe($e) => $t,
            Term::Id($e) => $t,
            Term::Finite($e) => $t,
            Term::Ix($e) => $t,
            Term::Nat($e) => $t,
            Term::Natural($e) => $t,
        }
    };
}

/// A trait implemented by values in the dependent type theory underlying `isotope`
pub trait Value: PartialEq + Eq + PartialEq<Term> + Hash + Code + Sized {
    /// Convert this value to a `TermId`
    #[inline]
    fn into_id(self) -> TermId {
        TermId::new_direct(self.into_term())
    }
    /// Convert this value to a term
    fn into_term(self) -> Term;
    /// Get the type of this term
    fn ty(&self) -> CTerm;
    /// Typecheck a term
    fn tyck(&self) -> bool {
        self.tyck_with(&mut TyCtx::default())
    }
    /// Typecheck a term within a given typing context, returning the first error index, if any
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool;
    /// Check whether this term is a type
    fn is_ty(&self) -> bool {
        matches!(&*self.ty(), Term::Universe(_))
    }
    /// Get the inductor type for this type given a family and a list of branches
    fn ind_ty(&self, _family: &TermId, _branches: &[TermId]) -> Result<TermId, Error> {
        Err(Error::NotInductive)
    }
    /// Apply the inductor for this type (family) to a list of type parameters and an argument, given a list of branches
    /// Return `None` if this application cannot be reduced further.
    fn ind(
        &self,
        _family: &TermId,
        _branches: &[TermId],
        _params: &[TermId],
        _arg: &TermId,
    ) -> Result<Option<TermId>, Error> {
        Err(Error::NotInductive)
    }
    /// If this term is a type, return it's typing universe; otherwise, return `None`
    fn universe(&self) -> Option<Universe> {
        match &*self.ty() {
            Term::Universe(u) => Some(*u),
            _ => None,
        }
    }
    /// Get the current flags of this term
    fn load_flags(&self) -> Flags;
    /// Get the "free variable bound" of this term, i.e., the highest free-variable index in this term plus one
    fn fvb(&self) -> u64;
    /// Shift this term's free variables above `base` backwards by `n`. Return the modified term, if any
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error>;
    /// Substitute and evaluate a term within a context. The context determines reduction mode, etc.
    /// Returns `None` if there is no change.
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error>;
}

impl Value for Term {
    #[inline]
    fn into_id(self) -> TermId {
        for_term!(self; t => t.into_id())
    }
    #[inline]
    fn into_term(self) -> Term {
        self
    }
    #[inline]
    fn ty(&self) -> CTerm {
        for_term!(self; t => t.ty())
    }
    #[inline]
    fn tyck(&self) -> bool {
        for_term!(self; t => t.tyck())
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        for_term!(self; t => t.tyck_with(ctx))
    }
    #[inline]
    fn is_ty(&self) -> bool {
        for_term!(self; t => t.is_ty())
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        for_term!(self; t => t.load_flags())
    }
    #[inline]
    fn fvb(&self) -> u64 {
        for_term!(self; t => t.fvb())
    }
    #[inline]
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.shift(n, base))
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.eval(ctx))
    }
    #[inline]
    fn ind_ty(&self, family: &TermId, branches: &[TermId]) -> Result<TermId, Error> {
        for_term!(self; t => t.ind_ty(family, branches))
    }
    #[inline]
    fn ind(
        &self,
        family: &TermId,
        branches: &[TermId],
        params: &[TermId],
        arg: &TermId,
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.ind(family, branches, params, arg))
    }
}

impl Code for Term {
    fn code(&self) -> u64 {
        for_term!(self; t => t.code())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn term_from_arc() {
        let direct = TermId::new_direct(Term::Nat(Nat));
        let arc = TermId::from_arc(Term::Nat(Nat).into());
        assert_eq!(direct, arc);
        assert_eq!(direct.code(), arc.code());
        assert!(direct.fast_eq(&arc));
        assert_ne!(direct.as_ptr(), arc.as_ptr());
        assert!(!direct.ptr_eq(&arc));
        assert_eq!(direct.as_term(), arc.as_term());
        assert_eq!(direct.as_term() as *const _, direct.as_ptr());
        assert_eq!(arc.as_term() as *const _, arc.as_ptr());

        let cloned_direct = direct.clone();
        let cloned_arc = arc.clone();
        assert_eq!(cloned_direct, direct);
        assert_eq!(cloned_arc, arc);
        assert_eq!(cloned_direct.as_ptr(), direct.as_ptr());
        assert_eq!(cloned_arc.as_ptr(), arc.as_ptr());

        let into_direct = cloned_direct.into_id();
        let into_arc = cloned_arc.into_id();
        assert_eq!(into_direct, direct);
        assert_eq!(into_arc, arc);
        assert_eq!(into_direct.as_ptr(), direct.as_ptr());
        assert_eq!(into_arc.as_ptr(), arc.as_ptr());
    }
    #[test]
    fn cterm_impl_value() {
        let nat = Nat.into_id();
        let bool_ = Finite(2).into_id();
        let false_ = Finite(2).ix(0).into_id();
        let true_ = Finite(2).ix(1).into_id();
        let cterms = [
            (&nat, CTerm::Id(&nat)),
            (&nat, CTerm::Term(Term::Nat(Nat))),
            (&bool_, CTerm::Id(&bool_)),
            (&bool_, CTerm::Term(Term::Finite(Finite(2)))),
            (&false_, CTerm::Id(&false_)),
            (&false_, CTerm::Term(Term::Ix(Finite(2).ix(0)))),
            (&true_, CTerm::Id(&true_)),
            (&true_, CTerm::Term(Term::Ix(Finite(2).ix(1)))),
        ];
        for (id, cterm) in &cterms {
            assert_eq!(**id, *cterm);
            assert_eq!(**id, cterm.clone().into_term());
            assert_eq!(id.ty(), cterm.ty());
            assert_eq!(id.tyck(), cterm.tyck());
            assert_eq!(
                id.tyck_with(&mut TyCtx::default()),
                cterm.tyck_with(&mut TyCtx::default())
            );
            assert_eq!(id.is_ty(), cterm.is_ty());
            assert_eq!(id.fvb(), cterm.fvb());
            assert_eq!(id.shift(1, 2), cterm.shift(1, 2));
            assert_eq!(
                id.eval(&mut EvalCtx::default()),
                cterm.eval(&mut EvalCtx::default())
            );
            assert_eq!(id.code(), cterm.code());
        }
    }
}

/*!
Flags for terms
*/
use std::ops::{BitOr, Mul};
use std::sync::atomic::{AtomicU8, Ordering};

//TODO: normal form flags, applicative/active flags...
//TODO: proper flag design based off 4-valued logic

/// Flags for a term
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd, Default)]
pub struct Flags(u8);

impl Flags {
    /// Set the given flags to the given values
    #[inline]
    pub const fn set_flags(self, flags: Flags) -> Flags {
        Flags(self.0 | flags.0)
    }
    /// Unset the given flags, setting them all to null
    #[inline]
    pub const fn unset_flags(self, flags: Flags) -> Flags {
        Flags(self.0 & !(flags.0 | (flags.0 & ODD_MASK) >> 1) | ((flags.0 & EVEN_MASK) << 1))
    }
    /// Join this set of flags with another
    #[inline]
    pub const fn join_flags(self, flags: Flags) -> Flags {
        Flags(((self.0 & EVEN_MASK) | (flags.0 & EVEN_MASK)) | (self.0 & flags.0))
    }
    /// Set that this term type-checks
    #[inline]
    pub const fn set_tyck(self, value: bool) -> Flags {
        Flags(self.0 | (TYCK_U8 << value as u32))
    }
    /// *Un*set that this term type-checks
    #[inline]
    pub const fn unset_tyck(self) -> Flags {
        Flags(self.0 & !TYCK_U8)
    }
    /// Set that this term is in beta normal form
    #[inline]
    pub const fn set_beta(self, value: bool) -> Flags {
        Flags(self.0 | (BETA_U8 << value as u32))
    }
    /// *Un*set that this term is in beta normal form
    #[inline]
    pub const fn unset_beta(self) -> Flags {
        Flags(self.0 & !BETA_U8)
    }
    /// Check whether this term definitely type checks
    #[inline]
    pub const fn tyck(self) -> bool {
        self.0 & (TYCK_U8 | (TYCK_U8 << 1)) == (TYCK_U8 << 1)
    }
    /// Check whether this term is invalid
    #[inline]
    pub const fn invalid(self) -> bool {
        (self.0 & TYCK_U8 == TYCK_U8) || !self.consistent()
    }
    /// Check whether this term is consistent
    #[inline]
    pub const fn consistent(self) -> bool {
        ((self.0 & ODD_MASK) >> 1) & (self.0 & EVEN_MASK) == 0
    }
}

impl BitOr for Flags {
    type Output = Flags;
    #[inline]
    fn bitor(self, other: Flags) -> Flags {
        self.set_flags(other)
    }
}

impl Mul for Flags {
    type Output = Flags;
    #[inline]
    fn mul(self, other: Flags) -> Flags {
        self.join_flags(other)
    }
}

/// The flags for a valid, normal term which is not necessarily a type
pub const NORMAL_TERM: Flags = TYCK.set_flags(BETA);
/// The flag that a term is in beta-normal form
pub const BETA: Flags = Flags(BETA_U8);

/// The flag that a term type-checks
pub const TYCK: Flags = Flags(TYCK_U8);

/// The flag that a term type-checks
const TYCK_U8: u8 = 0b00000001;

/// The flag that a term is in beta-normal form
const BETA_U8: u8 = 0b00000100;

/// Even bit mask
const EVEN_MASK: u8 = 0b01010101;

/// Odd bit mask
const ODD_MASK: u8 = 0b10101010;

/// A atomic flag
#[derive(Debug, Default)]
pub struct AtomicFlags(AtomicU8);

impl Clone for AtomicFlags {
    fn clone(&self) -> Self {
        Self::new(self.load_flags())
    }
}

impl AtomicFlags {
    /// Construct a new set of atomic flags from a given set of flags
    #[inline]
    pub const fn new(flags: Flags) -> AtomicFlags {
        AtomicFlags(AtomicU8::new(flags.0))
    }
    /// Load the current flags
    #[inline]
    pub fn load_flags(&self) -> Flags {
        Flags(self.0.load(Ordering::Relaxed))
    }
    /// Set the given flags, overwriting any old ones
    #[inline]
    pub fn store_flags(&self, flags: Flags) {
        self.0.store(flags.0, Ordering::Relaxed)
    }
    /// Set the given flags, returning whether a change was made
    #[inline]
    pub fn set_flags(&self, flags: Flags) -> bool {
        self.0.fetch_or(flags.0, Ordering::Relaxed) & flags.0 != flags.0
    }
    /// Set a given flag to a given value
    #[inline(always)]
    fn set_flag(&self, flag: u8, value: bool) -> bool {
        self.0.fetch_or(flag << value as u32, Ordering::Relaxed) & (flag | flag << 1)
            != (flag << value as u32)
    }
    /// Set whether this term type-checks, returning whether a change was made
    #[inline]
    pub fn set_tyck(&self, tyck: bool) -> bool {
        self.set_flag(TYCK_U8, tyck)
    }
    /// Set that this term is in beta normal form, returning whether a change was made
    #[inline]
    pub fn set_beta(&self, beta: bool) -> bool {
        self.set_flag(BETA_U8, beta)
    }
    /// Check whether this term type checks
    #[inline]
    pub fn tyck(&self) -> bool {
        self.load_flags().tyck()
    }
    /// Check whether this term is invalid
    #[inline]
    pub fn invalid(&self) -> bool {
        self.load_flags().invalid()
    }
}

impl From<Flags> for AtomicFlags {
    fn from(flags: Flags) -> AtomicFlags {
        AtomicFlags::new(flags)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn empty_flags() {
        let flags = Flags::default();
        assert!(!flags.invalid());
        assert!(!flags.tyck());
    }
    #[test]
    fn basic_consistency() {
        assert!(Flags::default().consistent());
        assert!(Flags::default().set_tyck(true).consistent());
        assert!(!Flags::default().tyck());
        assert!(Flags::default().set_tyck(true).tyck());
        assert!(!Flags::default().set_tyck(false).tyck());
        assert!(!Flags::default().set_tyck(true).set_tyck(false).tyck());
        assert!(Flags::default().set_tyck(false).consistent());
        assert!(!Flags::default().set_tyck(true).set_tyck(false).consistent());
        assert_eq!(
            Flags::default().set_tyck(true).set_tyck(false),
            Flags::default().set_tyck(false).set_tyck(true)
        );
        assert!(Flags::default().set_tyck(false).set_beta(true).consistent());
        assert!(!Flags::default()
            .set_beta(false)
            .set_tyck(false)
            .set_beta(true)
            .consistent());
    }
}

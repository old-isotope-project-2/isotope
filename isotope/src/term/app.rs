/*!
Function applications
*/
use super::*;

/// A function application
#[derive(Debug, Clone)]
pub struct App {
    /// The function being applied
    func: TermId,
    /// The argument the function is being applied to
    arg: TermId,
    /// The type of this application
    ty: TermId,
    /// The flags for this application
    flags: AtomicFlags,
    /// The code of this application
    code: u64,
    /// The free variable bound of this application
    fvb: u64,
}

impl App {
    /// Try to create a new function application
    pub fn try_new(func: TermId, arg: TermId) -> Result<App, Error> {
        let ty = match &*func.ty() {
            Term::Pi(pi) => pi.apply_ty(arg.clone())?,
            _ => return Err(Error::NotAFunction),
        };
        let func_fvb = func.fvb();
        let arg_fvb = arg.fvb();
        let flags = if func_fvb == 0 || arg_fvb == 0 {
            func.load_flags().join_flags(arg.load_flags())
        } else {
            Flags::default()
        }
        .into();
        let fvb = func_fvb.max(arg_fvb);
        let mut result = App {
            func,
            arg,
            ty,
            flags,
            fvb,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Update the code of this function application
    pub fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(5722, 773);
        self.hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Get the function being applied by this application
    pub fn func(&self) -> &TermId {
        &self.func
    }
    /// Get the argument of this application
    pub fn arg(&self) -> &TermId {
        &self.arg
    }
    /// Get the local beta-redex of this application, or `None` if not applicable
    pub fn get_local_beta_redex(&self) -> Result<Option<TermId>, Error> {
        match &*self.func {
            Term::Lambda(l) => l.apply(self.arg.clone()).map(Some),
            _ => Ok(None),
        }
    }
}

impl Value for App {
    #[inline]
    fn into_term(self) -> Term {
        Term::App(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Id(&self.ty)
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        let flags = self.flags.load_flags();
        let is_null = ctx.is_null(self);
        if flags.invalid() {
            return false;
        } else if flags.tyck() && is_null {
            return true;
        }
        let fail = || {
            if is_null {
                self.flags.set_tyck(false);
            }
            false
        };

        let tyck_func = self.func.tyck_with(ctx);
        if !tyck_func {
            return fail();
        }
        let tyck_arg = self.arg.tyck_with(ctx);
        if !tyck_arg {
            return fail();
        }

        // If it type checks in *one* context, it type checks in the null context
        self.flags.set_tyck(true);

        return true;
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        self.fvb
    }
    #[inline]
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        if base >= self.fvb {
            return Ok(None);
        }
        let func = self
            .func
            .shift(n, base)?
            .unwrap_or_else(|| self.func.clone());
        let arg = self.arg.shift(n, base)?.unwrap_or_else(|| self.arg.clone());
        let ty = self.ty.shift(n, base)?.unwrap_or_else(|| self.ty.clone());
        let flags = self.flags.clone();
        let fvb = arg.fvb().max(func.fvb()).max(ty.fvb());
        let mut app = App {
            func,
            arg,
            ty,
            flags,
            fvb,
            code: 0,
        };
        app.update_code();
        Ok(Some(app.into_id()))
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if self.fvb() == 0 {
            return Ok(None);
        }
        let func = self.func.eval(ctx)?;
        let arg = self.arg.eval(ctx)?;
        if func.is_none() && arg.is_none() {
            //TODO: is this *always* a bug?
            return Ok(None);
        }
        let func = func.unwrap_or_else(|| self.func.clone());
        let arg = arg.unwrap_or_else(|| self.arg.clone());
        let result = App::try_new(func, arg)?.into_id();
        Ok(Some(result))
    }
}

impl Eq for App {}

impl PartialEq for App {
    fn eq(&self, other: &App) -> bool {
        self.code == other.code
            && self.arg == other.arg
            && self.func == other.func
            && self.ty == other.ty
    }
}

impl Hash for App {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.func.hash(state);
        self.arg.hash(state);
        self.ty.hash(state);
    }
}

impl PartialEq<Term> for App {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::App(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Code for App {
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

#[cfg(test)]
mod test {
    use num::BigUint;

    use super::*;
    #[test]
    fn id_app() {
        let id = Lambda::id(Nat.into_id()).unwrap().into_id();
        let n = BigUint::from(5u64).into_id();
        let n_app = App::try_new(id, n).unwrap();
        assert_eq!(Nat, *n_app.ty());
        assert_eq!(n_app.fvb(), 0);
        assert!(n_app.tyck());
    }
    #[test]
    fn invalid_app_does_not_typecheck() {
        let x = Var::try_new(0, Nat.into_id()).unwrap().into_id();
        let f_ty = Pi::simple(Nat.into_id(), Nat.into_id()).unwrap().into_id();
        let f = Var::try_new(0, f_ty).unwrap().into_id();
        let s = App::try_new(f, x).unwrap();
        assert!(!s.tyck());
        assert!(!s.tyck());
    }
}

/*!
Dependent function types
*/

use super::*;

/// A dependent function type
#[derive(Debug, Clone)]
pub struct Pi {
    /// The type this function type is being parametrized by
    arg_ty: TermId,
    /// The result type of this function type
    result_ty: TermId,
    /// The type of this function type, which is always a typing universe
    ty: Universe,
    /// The flags of this function type
    flags: AtomicFlags,
    /// The code of this function type
    code: u64,
    /// The free variable bound of this function type
    fvb: u64,
}

impl Pi {
    /// Try to construct a new, typechecked dependent function type
    pub fn checked_new(arg_ty: TermId, result: TermId) -> Result<Pi, Error> {
        let new = Self::try_new(arg_ty, result)?;
        if new.tyck() {
            Ok(new)
        } else {
            Err(Error::TyckFailure)
        }
    }
    /// Try to construct a new dependent function type
    pub fn try_new(arg_ty: TermId, result_ty: TermId) -> Result<Pi, Error> {
        let arg_universe = arg_ty.universe().ok_or(Error::NotAType)?;
        let result_universe = result_ty.universe().ok_or(Error::NotAType)?;
        let ty = arg_universe.max(result_universe.get_universe());
        //TODO: smarter flags...
        let result_fvb = result_ty.fvb();
        let fvb = arg_ty.fvb().max(result_fvb.saturating_sub(1));
        let flags = if fvb == 0 && result_fvb == 0 {
            arg_ty.load_flags().join_flags(result_ty.load_flags())
        } else {
            Flags::default()
        }
        .into();
        let mut result = Pi {
            arg_ty,
            result_ty,
            ty,
            flags,
            fvb,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Try to construct a constant function type
    pub fn simple(arg_ty: TermId, result_ty: TermId) -> Result<Pi, Error> {
        Self::try_new(arg_ty, result_ty.shift(1, 0)?.unwrap_or(result_ty))
    }
    /// Get a unary function over a given type. Return an error if it is not actually a type
    pub fn unary(arg_ty: TermId) -> Result<Pi, Error> {
        let ty = arg_ty.universe().ok_or(Error::NotAType)?.into();
        let fvb = arg_ty.fvb();
        let flags = arg_ty.load_flags().into();
        let result_ty = arg_ty.shift(1, 0)?.unwrap_or_else(|| arg_ty.clone());
        let mut result = Pi {
            arg_ty,
            result_ty,
            ty,
            flags,
            fvb,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Update the code of this dependent function type
    fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(84, 139);
        self.hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Apply this function type to an argument
    pub fn apply_ty(&self, arg: TermId) -> Result<TermId, Error> {
        self.apply_ty_with(arg, &mut EvalCtx::default())
    }
    /// Apply this function type to an argument within a given evaluation context, yielding the result type
    pub fn apply_ty_with(&self, arg: TermId, ctx: &mut EvalCtx) -> Result<TermId, Error> {
        ctx.push(arg);
        let result = self
            .result_ty
            .eval(ctx)?
            .unwrap_or_else(|| self.result_ty.clone());
        ctx.pop();
        Ok(result)
    }
    /// Get the argument type for this pi type
    pub fn arg_ty(&self) -> &TermId {
        &self.arg_ty
    }
    /// Get the result type for this pi type
    pub fn result_ty(&self) -> &TermId {
        &self.result_ty
    }
}

impl Value for Pi {
    #[inline]
    fn into_term(self) -> Term {
        Term::Pi(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Term(Term::Universe(self.ty))
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        let flags = self.flags.load_flags();
        let is_null = ctx.is_null(self);
        if flags.invalid() {
            return false;
        } else if flags.tyck() && is_null {
            return true;
        }
        let fail = || {
            if is_null {
                self.flags.set_tyck(false);
            }
            false
        };
        if !self.arg_ty.tyck_with(ctx) {
            return fail();
        }
        if ctx.try_push(self.arg_ty.clone()).is_err() {
            return fail();
        }
        if !self.result_ty.tyck_with(ctx) {
            return fail();
        }
        self.flags.set_tyck(true);
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        if n == 0 || base > self.fvb {
            return Ok(None);
        }
        let arg_ty = self
            .arg_ty
            .shift(n, base)?
            .unwrap_or_else(|| self.arg_ty.clone());
        let result_ty = self
            .result_ty
            .shift(n, base + 1)?
            .unwrap_or_else(|| self.result_ty.clone());
        let fvb = arg_ty.fvb().max(result_ty.fvb().saturating_sub(1));
        let mut result = Pi {
            arg_ty,
            result_ty,
            fvb,
            ty: self.ty.clone(),
            flags: self.flags.clone(),
            code: 0,
        };
        result.update_code();
        Ok(Some(result.into_id()))
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if self.fvb == 0 {
            return Ok(None);
        }
        let arg_ty = self
            .arg_ty
            .eval(ctx)?
            .unwrap_or_else(|| self.arg_ty.clone());
        ctx.push_binding(arg_ty.clone())?;
        let result_ty = self
            .result_ty
            .eval(ctx)?
            .unwrap_or_else(|| self.result_ty.clone());
        let binding = ctx.pop_binding();
        debug_assert_eq!(binding.unwrap().0, arg_ty);
        let result = Pi::try_new(arg_ty, result_ty)?.into_id();
        Ok(Some(result))
    }
}

impl PartialEq for Pi {
    #[inline]
    fn eq(&self, other: &Pi) -> bool {
        self.code == other.code && self.arg_ty == other.arg_ty && self.result_ty == other.result_ty
    }
}

impl PartialEq<Term> for Pi {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Pi(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Eq for Pi {}

impl Hash for Pi {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.arg_ty.hash(state);
        self.result_ty.hash(state);
    }
}

impl Code for Pi {
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

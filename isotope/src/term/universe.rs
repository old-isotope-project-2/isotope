/*!
Leveled typing universes
*/
use super::*;
/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct Universe {
    /// The level of this typing universe
    level: u64,
}

/// The universe of sets; the smallest typing universe
pub const SET: Term = Term::Universe(Universe::SET);
/// The universe of simple types
pub const TYPE: Term = Term::Universe(Universe::TYPE);

impl Universe {
    /// The universe of sets; the smallest typing universe
    pub const SET: Universe = Universe::new(0);
    /// The universe of simple types
    pub const TYPE: Universe = Universe::new(1);
    /// Create a new typing universe at a given level
    #[inline]
    pub const fn new(level: u64) -> Universe {
        Universe { level }
    }
    /// Get the type of this typing universe
    #[inline]
    pub const fn get_universe(&self) -> Universe {
        Universe {
            level: self.level + 1,
        }
    }
}

impl Value for Universe {
    #[inline]
    fn into_term(self) -> Term {
        Term::Universe(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Term(Term::Universe(self.get_universe()))
    }
    #[inline]
    fn tyck(&self) -> bool {
        true
    }
    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        true
    }
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        NORMAL_TERM
    }
    #[inline]
    fn fvb(&self) -> u64 {
        0
    }
    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    #[inline]
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
}

impl PartialEq<Term> for Universe {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Universe(u) => self.eq(u),
            _ => false,
        }
    }
}

impl Code for Universe {
    fn code(&self) -> u64 {
        let mut hasher = AHasher::new_with_keys(452, 596);
        self.level.hash(&mut hasher);
        hasher.finish()
    }
}

impl From<Universe> for Term {
    #[inline]
    fn from(u: Universe) -> Term {
        u.into_term()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_universe_properties() {
        assert_eq!(Universe::SET, Universe::SET);
        assert_eq!(Universe::TYPE, Universe::TYPE);
        assert_ne!(Universe::SET, Universe::TYPE);
        assert_eq!(Universe::SET, SET);
        assert_eq!(Universe::TYPE, TYPE);
        assert_ne!(Universe::SET, TYPE);
        assert_ne!(Universe::TYPE, SET);
        assert_eq!(SET.fvb(), 0);
        assert_eq!(TYPE.fvb(), 0);
        assert_eq!(*SET.ty(), TYPE);
        assert_eq!(SET.shift(1, 0), Ok(None));
        assert!(SET.is_ty());
        assert_eq!(Universe::SET.get_universe(), Universe::TYPE);
        assert_eq!(SET.universe().unwrap(), TYPE);
    }
}

/*!
Variables, implemented using de-Bruijn indexing
*/

use super::*;

/// A descriptor for a variable, given by a de-Bruijn index
#[derive(Debug, Clone)]
pub struct Var {
    ix: u64,
    ty: TermId,
    code: u64,
    flags: AtomicFlags,
    fvb: u64,
}

impl Var {
    /// Create a new variable with a given index and type
    #[inline]
    pub fn try_new(ix: u64, ty: TermId) -> Result<Var, Error> {
        // Type must actually be a type
        let ty_fvb = ty.fvb();
        if ty_fvb == ix + 1 {
            return Err(Error::CyclicType);
        }
        if !ty.is_ty() {
            return Err(Error::NotAType);
        }
        let flags = Flags::default().into();
        let fvb = ty.fvb().max(ix + 1);
        let mut result = Var {
            ix,
            ty,
            fvb,
            code: 0,
            flags,
        };
        result.update_code();
        Ok(result)
    }
    /// Update the code of a variable
    #[inline]
    fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(2542, 5232);
        self.hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Get the index of this variable
    pub fn ix(&self) -> u64 {
        self.ix
    }
}

impl Value for Var {
    #[inline]
    fn into_term(self) -> Term {
        Term::Var(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Id(&self.ty)
    }
    #[inline]
    fn tyck(&self) -> bool {
        let flags = self.flags.load_flags();
        let result = if flags.invalid() {
            false
        } else if flags.tyck() {
            true
        } else {
            self.ty.tyck()
        };
        debug_assert_eq!(result, self.tyck_with(&mut TyCtx::default()));
        result
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        let flags = self.flags.load_flags();
        let is_null = ctx.is_null(self);
        if flags.invalid() {
            return false;
        }
        let fail = || {
            if is_null {
                self.flags.set_tyck(false);
            }
            false
        };
        if !(flags.tyck() || self.ty.tyck_with(ctx)) {
            return fail();
        }
        if ctx.constrain(self.ix, &self.ty).is_err() {
            return fail();
        }
        self.flags.set_tyck(true);
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        self.fvb
    }
    #[inline]
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        if n == 0 || base >= self.fvb {
            return Ok(None);
        }
        let ix = Error::checked_shift(self.ix, n, base)?;
        let ty = self.ty.shift(n, base)?.unwrap_or_else(|| self.ty.clone());
        let fvb = ty.fvb().max(ix + 1);
        let flags = self.flags.clone();
        let mut result = Var {
            ix,
            ty,
            fvb,
            code: 0,
            flags,
        };
        result.update_code();
        Ok(Some(result.into_id()))
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        //TODO: flags and such, optimization...
        let result = match ctx.ix(self.ix) {
            Either::Left(term) => {
                if !ctx.permissive {
                    if term.ty() != self.ty {
                        return Err(Error::TypeMismatch);
                    }
                }
                term
            }
            Either::Right(ix) if ix == self.ix => return Ok(None),
            Either::Right(ix) => {
                Var::try_new(ix, self.ty.eval(ctx)?.unwrap_or_else(|| self.ty.clone()))?.into_id()
            }
        };
        Ok(Some(result))
    }
}

impl PartialEq<Term> for Var {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Var(other) => self.eq(other),
            _ => false,
        }
    }
}

impl PartialEq for Var {
    fn eq(&self, other: &Var) -> bool {
        self.ix == other.ix && self.ty == other.ty
    }
}

impl Eq for Var {}

impl Code for Var {
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

impl Hash for Var {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.ix.hash(hasher);
        self.ty.hash(hasher);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_var_properties() {
        let a = Var::try_new(1, SET.into_id()).unwrap();
        assert_eq!(*a.ty(), SET);
        assert_ne!(a, SET);
        assert!(a.is_ty());
        let at = a.clone().into_term();
        assert_eq!(a, at);
        assert_eq!(*at.ty(), SET);
        assert_ne!(at, SET);
        assert!(at.is_ty());
        assert_eq!(a.fvb(), 2);
        assert_eq!(at.fvb(), 2);
        let x = Var::try_new(0, at.into_id()).unwrap();
        assert_eq!(a, *x.ty());
        assert_ne!(x, a);
        assert_eq!(x.fvb(), 2);
        assert_eq!(x.fvb(), 2);
        assert!(!x.is_ty());
    }
}

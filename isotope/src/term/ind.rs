/*!
Induction
*/
use super::*;

/// An instance of induction
#[derive(Debug, Clone)]
pub struct Ind {
    /// The data defining this gamma node, consisting of branches followed by base type, family, and type.
    data: Vec<TermId>,
    /// The code of this gamma node
    code: u64,
    /// The free variable bound of this gamma node
    fvb: u64,
    /// The flags of this gamma node
    flags: AtomicFlags,
}

impl Ind {
    /// Create a new gamma node
    pub fn new(base: TermId, family: TermId, branches: Vec<TermId>) -> Result<Ind, Error> {
        let ty = base.ind_ty(&family, &branches[..])?;
        let mut data = branches;
        data.push(base);
        data.push(family);
        data.push(ty);
        let fvb = data
            .iter()
            .map(|data| data.fvb())
            .max()
            .expect("Data vector is nonempty");
        //TODO: smarter flags
        let flags = AtomicFlags::default();
        let mut result = Ind {
            data,
            fvb,
            flags,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// The branches of this gamma node
    pub fn branches(&self) -> &[TermId] {
        &self.data[..self.data.len() - 3]
    }
    /// The family of this gamma node
    pub fn family(&self) -> &TermId {
        &self.data[self.data.len() - 2]
    }
    /// The base type (family) of this gamma node
    pub fn base(&self) -> &TermId {
        &self.data[self.data.len() - 3]
    }
    /// Get the type of this gamma node as an `&TermId`
    pub fn get_ty(&self) -> &TermId {
        &self.data[self.data.len() - 1]
    }
    /// Apply this gamma node to a given term with a given set of type parameters. Return `None` if it cannot be reduced further
    pub fn apply_ind(&self, params: &[TermId], arg: &TermId) -> Result<Option<TermId>, Error> {
        self.base().ind(self.family(), self.branches(), params, arg)
    }
    /// Update the code of this gamma node
    fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(226, 226);
        self.hash(&mut hasher);
        self.code = hasher.finish()
    }
}

impl Value for Ind {
    fn into_term(self) -> Term {
        Term::Ind(self)
    }

    fn ty(&self) -> CTerm {
        CTerm::Id(self.get_ty())
    }

    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        todo!()
    }

    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }

    fn fvb(&self) -> u64 {
        self.fvb
    }

    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        if n == 0 || self.fvb <= base {
            return Ok(None);
        }
        let data: Vec<TermId> = self
            .data
            .iter()
            .map(|data| data.shift_clone(n, base))
            .collect::<Result<_, _>>()?;
        let fvb = data
            .iter()
            .map(|data| data.fvb())
            .max()
            .expect("Data is not empty");
        let flags = self.flags.clone();
        let mut result = Ind {
            data,
            fvb,
            flags,
            code: 0,
        };
        result.update_code();
        Ok(Some(result.into_id()))
    }

    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if self.fvb == 0 {
            return Ok(None);
        }
        let data = self
            .data
            .iter()
            .map(|data| {
                data.eval(ctx)
                    .transpose()
                    .unwrap_or_else(|| Ok(data.clone()))
            })
            .collect::<Result<_, _>>()?;
        let fvb = self
            .data
            .iter()
            .map(|data| data.fvb())
            .max()
            .expect("Data is not empty");
        let flags = self.flags.clone();
        let mut result = Ind {
            data,
            fvb,
            flags,
            code: 0,
        };
        result.update_code();
        Ok(Some(result.into_id()))
    }
}

impl Eq for Ind {}

impl PartialEq for Ind {
    fn eq(&self, other: &Ind) -> bool {
        self.code == other.code && self.data == other.data
    }
}

impl PartialEq<Term> for Ind {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Ind(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Hash for Ind {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.data.hash(state)
    }
}

impl Code for Ind {
    fn code(&self) -> u64 {
        self.code
    }
}

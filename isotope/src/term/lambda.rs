/*!
Lambda functions
*/
use super::*;

/// A lambda function
#[derive(Debug, Clone)]
pub struct Lambda {
    /// The type this function is being parametrized by
    arg_ty: TermId,
    /// The result of this function
    result: TermId,
    /// The type of this function
    ty: TermId,
    /// The flags of this function
    flags: AtomicFlags,
    /// The code of this function
    code: u64,
    /// The free variable bound of this function
    fvb: u64,
}

impl Lambda {
    /// Try to construct a new, typechecked lambda function
    pub fn checked_new(arg_ty: TermId, result: TermId) -> Result<Lambda, Error> {
        let new = Self::try_new(arg_ty, result)?;
        if new.tyck() {
            Ok(new)
        } else {
            Err(Error::TyckFailure)
        }
    }
    /// Try to construct a new function
    pub fn try_new(arg_ty: TermId, result: TermId) -> Result<Lambda, Error> {
        let ty = Pi::try_new(arg_ty.clone(), result.ty().into_id())?.into_id();
        //TODO: smarter flags...
        let result_fvb = result.fvb();
        let fvb = arg_ty.fvb().max(result_fvb.saturating_sub(1));
        let flags = if fvb == 0 && result_fvb == 0 {
            arg_ty.load_flags().join_flags(result.load_flags())
        } else {
            Flags::default()
        }
        .into();
        let mut result = Lambda {
            arg_ty,
            result,
            ty,
            flags,
            fvb,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Get the argument type of this lambda function
    #[inline]
    pub fn arg_ty(&self) -> &TermId {
        &self.arg_ty
    }
    /// Get the result of this lambda function
    #[inline]
    pub fn result(&self) -> &TermId {
        &self.result
    }
    /// Construct a constant lambda function
    pub fn constant(arg_ty: TermId, result: TermId) -> Result<Lambda, Error> {
        Self::try_new(arg_ty, result.shift(1, 0)?.unwrap_or(result))
    }
    /// Construct the identity function with a given argument type
    pub fn id(arg_ty: TermId) -> Result<Lambda, Error> {
        let ty = Pi::unary(arg_ty.clone())?.into_id();
        let shifted_arg = arg_ty.shift(1, 0)?.unwrap_or_else(|| arg_ty.clone());
        let result = Var::try_new(0, shifted_arg)
            .expect("arg_ty is a type, and due to shifting it cannot depend on index 0")
            .into_id();
        let fvb = arg_ty.fvb();
        let flags = arg_ty.load_flags().into();
        let mut result = Lambda {
            arg_ty,
            result,
            ty,
            flags,
            fvb,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Construct the lambda function which eta-reduces to the argument. The argument must have a function type
    pub fn backwards_eta(f: TermId) -> Result<Lambda, Error> {
        let arg_ty = match &*f.ty() {
            Term::Pi(p) => p.arg_ty().clone(),
            _ => return Err(Error::NotAFunction),
        };
        let arg = Var::try_new(0, arg_ty.shift_clone(1, 0)?)?;
        let f_inner = f.shift_move(1, 0)?;
        let app = App::try_new(f_inner, arg.into_id())?;
        Lambda::try_new(arg_ty, app.into_id())
    }
    /// Get the function composition operator between three types
    pub fn compose(a: TermId, b: TermId, c: TermId) -> Result<Lambda, Error> {
        let a1 = a.shift_clone(1, 0)?;
        let a2 = a.shift_clone(2, 0)?;
        let a3 = a.shift_clone(3, 0)?;
        let b2 = b.shift_clone(2, 0)?;
        let b3 = b.shift_clone(3, 0)?;
        let b4 = b.shift_clone(4, 0)?;
        let c1 = c.shift_clone(1, 0)?;
        let c4 = c.shift_clone(4, 0)?;
        let tf3 = Pi::try_new(b3.clone(), c4.clone())?.into_id();
        let f = Var::try_new(2, tf3)?.into_id();
        let tg3 = Pi::try_new(a3.clone(), b4.clone())?.into_id();
        let g = Var::try_new(1, tg3)?.into_id();
        let x = Var::try_new(0, a3.clone())?.into_id();
        debug_assert_eq!(x.ty(), a3);
        let gx = App::try_new(g, x)?.into_id();
        debug_assert_eq!(gx.ty(), b3);
        let fgx = App::try_new(f, gx)?.into_id();
        let x_fgx = Lambda::try_new(a2, fgx)?.into_id();
        let tg1 = Pi::try_new(a1, b2)?.into_id();
        let gx_fgx = Lambda::try_new(tg1, x_fgx)?.into_id();
        let tf = Pi::try_new(b, c1)?.into_id();
        Lambda::try_new(tf, gx_fgx)
    }
    /// Get the function composition for a typing universe
    pub fn compose_universe(u: TermId) -> Result<Lambda, Error> {
        // Note: universes never need to be shifted
        let a = Var::try_new(2, u.clone())?.into_id();
        let b = Var::try_new(1, u.clone())?.into_id();
        let c = Var::try_new(0, u.clone())?.into_id();
        let cmp = Self::compose(a, b, c)?.into_id();
        let c_cmp = Self::try_new(u.clone(), cmp)?.into_id();
        let bc_cmp = Self::try_new(u.clone(), c_cmp)?.into_id();
        Self::try_new(u.clone(), bc_cmp)
    }
    /// Update the code of this function
    fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(84, 139);
        self.hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Apply this function to an argument
    pub fn apply(&self, arg: TermId) -> Result<TermId, Error> {
        self.apply_with(arg, &mut EvalCtx::default())
    }
    /// Apply this function to an argument within a given evaluation context
    pub fn apply_with(&self, arg: TermId, ctx: &mut EvalCtx) -> Result<TermId, Error> {
        ctx.push(arg);
        let result = self
            .result
            .eval(ctx)?
            .unwrap_or_else(|| self.result.clone());
        ctx.pop();
        Ok(result)
    }
    /// Eta-reduce this lambda function. Return `None` if not of the form `#lambda x => f x` where `f` does not depend on `x`.
    pub fn get_local_eta_redex(&self) -> Option<TermId> {
        match &*self.result {
            Term::App(app) => match &**app.arg() {
                Term::Var(var) if var.ix() == 0 => {
                    return Some(app.func().shift_clone(-1, 0).ok()?);
                }
                _ => {}
            },
            _ => {}
        }
        None
    }
}

impl Value for Lambda {
    #[inline]
    fn into_term(self) -> Term {
        Term::Lambda(self)
    }
    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Id(&self.ty)
    }
    #[inline]
    fn tyck_with(&self, ctx: &mut TyCtx) -> bool {
        let flags = self.flags.load_flags();
        let is_null = ctx.is_null(self);
        if flags.invalid() {
            return false;
        } else if flags.tyck() && is_null {
            return true;
        }
        let fail = || {
            if is_null {
                self.flags.set_tyck(false);
            }
            false
        };
        if !self.arg_ty.tyck_with(ctx) {
            return fail();
        }
        if ctx.try_push(self.arg_ty.clone()).is_err() {
            return fail();
        }
        if !self.result.tyck_with(ctx) {
            return fail();
        }
        self.flags.set_tyck(true);
        true
    }
    #[inline]
    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }
    #[inline]
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn shift(&self, n: i64, base: u64) -> Result<Option<TermId>, Error> {
        if n == 0 || base >= self.fvb {
            return Ok(None);
        }
        let arg_ty = self
            .arg_ty
            .shift(n, base)?
            .unwrap_or_else(|| self.arg_ty.clone());
        let result = self
            .result
            .shift(n, base + 1)?
            .unwrap_or_else(|| self.result.clone());
        let fvb = arg_ty.fvb().max(result.fvb().saturating_sub(1));
        let ty = self.ty.shift(n, base)?.unwrap_or_else(|| self.ty.clone());
        let mut result = Lambda {
            arg_ty,
            result,
            fvb,
            ty,
            flags: self.flags.clone(),
            code: 0,
        };
        result.update_code();
        Ok(Some(result.into_id()))
    }
    #[inline]
    fn eval(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if self.fvb == 0 {
            return Ok(None);
        }
        let arg_ty = self
            .arg_ty
            .eval(ctx)?
            .unwrap_or_else(|| self.arg_ty.clone());
        ctx.push_binding(arg_ty.clone())?;
        let result = self
            .result
            .eval(ctx)?
            .unwrap_or_else(|| self.result.clone());
        let binding = ctx.pop_binding();
        debug_assert_eq!(binding.unwrap().0, arg_ty);
        let result = Lambda::try_new(arg_ty, result)?.into_id();
        Ok(Some(result))
    }
}

impl PartialEq for Lambda {
    #[inline]
    fn eq(&self, other: &Lambda) -> bool {
        self.code == other.code && self.arg_ty == other.arg_ty && self.result == other.result
    }
}

impl PartialEq<Term> for Lambda {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Lambda(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Eq for Lambda {}

impl Hash for Lambda {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.arg_ty.hash(state);
        self.result.hash(state);
    }
}

impl Code for Lambda {
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn finite_2_id() {
        let bool_ty = Finite(2);
        let t = bool_ty.ix(1).into_id();
        let f = bool_ty.ix(0).into_id();
        let id = Lambda::id(bool_ty.into_id()).expect("bool_ty is a type");
        assert!(id.tyck());
        let bools = [t, f];
        for b in &bools {
            assert_eq!(
                id.apply(b.clone()).expect("This is a valid application"),
                *b
            )
        }
    }
    #[test]
    fn invalid_lambda_does_not_typecheck() {
        let x = Var::try_new(0, Nat.into_id()).unwrap().into_id();
        let l = Lambda::try_new(SET.into_id(), x).unwrap();
        assert!(!l.tyck());
    }
    #[test]
    fn composition() {
        let s = SET.into_id();
        let svc = |ix| Var::try_new(ix, s.clone()).unwrap().into_id();
        let sv = [svc(0), svc(1), svc(2), svc(3), svc(4), svc(5)];
        let tf = Pi::try_new(sv[1].clone(), sv[1].clone()).unwrap().into_id();
        let f = Var::try_new(2, tf.shift(3, 0).unwrap().unwrap())
            .unwrap()
            .into_id();
        let tg = Pi::try_new(sv[2].clone(), sv[2].clone()).unwrap().into_id();
        let g = Var::try_new(1, tg.shift(3, 0).unwrap().unwrap())
            .unwrap()
            .into_id();
        let x = Var::try_new(0, sv[5].clone()).unwrap().into_id();
        assert_eq!(x.ty(), sv[5]);
        assert_eq!(x.fvb(), 6);
        let gx = App::try_new(g, x).unwrap().into_id();
        assert_eq!(gx.ty(), sv[4]);
        assert_eq!(gx.fvb(), 6);
        let fgx = App::try_new(f, gx).unwrap().into_id();
        assert_eq!(fgx.ty(), sv[3]);
        assert_eq!(fgx.fvb(), 6);
        let x_fgx = Lambda::try_new(sv[4].clone(), fgx).unwrap().into_id();
        assert_eq!(x_fgx.fvb(), 5);
        let gx_fgx = Lambda::try_new(tg.shift(1, 0).unwrap().unwrap(), x_fgx)
            .unwrap()
            .into_id();
        assert_eq!(gx_fgx.fvb(), 4);
        let cmp = Lambda::try_new(tf, gx_fgx).unwrap().into_id();
        assert_eq!(cmp.fvb(), 3);

        let l_cmp = Lambda::compose(sv[2].clone(), sv[1].clone(), sv[0].clone()).unwrap();

        assert_eq!(l_cmp, *cmp);

        let c_cmp = Lambda::try_new(s.clone(), cmp).unwrap().into_id();
        assert_eq!(c_cmp.fvb(), 2);
        let bc_cmp = Lambda::try_new(s.clone(), c_cmp).unwrap().into_id();
        assert_eq!(bc_cmp.fvb(), 1);
        let gen_cmp = Lambda::try_new(s.clone(), bc_cmp).unwrap().into_id();
        assert_eq!(gen_cmp.fvb(), 0);

        let l_gen_cmp = Lambda::compose_universe(s.clone()).unwrap();
        assert_eq!(l_gen_cmp, *gen_cmp);

        /*
        let bool_ty = Finite(2);
        let t = bool_ty.ix(1).into_id();
        let f = bool_ty.ix(0).into_id();
        let id = Lambda::id(bool_ty.into_id()).expect("bool_ty is a type");

        //TODO: application/beta reduction test
        */
    }
    #[test]
    fn eta_backwards_eta_is_id() {
        let f_ty = Pi::unary(Nat.into_id()).unwrap();
        let f = Var::try_new(0, f_ty.into_id()).unwrap().into_id();
        let f_eta = Lambda::backwards_eta(f.clone()).unwrap();
        assert_eq!(Nat, **f_eta.arg_ty());
        assert_eq!(f_eta.get_local_eta_redex().unwrap(), f);
    }
}

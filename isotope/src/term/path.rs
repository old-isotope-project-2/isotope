/*!
Identity types and beta reduction
*/
use super::*;

/// The type family of paths between two terms in a given type
#[derive(Debug, Clone)]
pub struct Id {
    base_ty: TermId,
    ty: Universe,
    code: u64,
    fvb: u64,
    flags: AtomicFlags,
}

impl Id {
    /// Try to construct a new path type family
    pub fn try_new(base_ty: TermId) -> Result<Id, Error> {
        let ty = base_ty.universe().ok_or(Error::NotAType)?;
        let fvb = ty.fvb();
        let flags = ty.load_flags().into();
        let mut result = Id {
            base_ty,
            ty,
            fvb,
            flags,
            code: 0,
        };
        result.update_code();
        Ok(result)
    }
    /// Update the code of this type
    fn update_code(&mut self) {
        let mut hasher = AHasher::new_with_keys(67023, 4514675);
        self.hash(&mut hasher);
        self.code = hasher.finish()
    }
}

impl Value for Id {
    #[inline]
    fn into_term(self) -> Term {
        Term::Id(self)
    }

    #[inline]
    fn ty(&self) -> CTerm {
        CTerm::Term(Term::Universe(self.ty))
    }

    #[inline]
    fn tyck_with(&self, _ctx: &mut TyCtx) -> bool {
        todo!()
    }

    #[inline]
    fn load_flags(&self) -> Flags {
        self.flags.load_flags()
    }

    #[inline]
    fn fvb(&self) -> u64 {
        self.fvb
    }

    #[inline]
    fn shift(&self, _n: i64, _base: u64) -> Result<Option<TermId>, Error> {
        todo!()
    }

    #[inline]
    fn eval(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        todo!()
    }
}

impl Eq for Id {}

impl PartialEq for Id {
    #[inline]
    fn eq(&self, other: &Id) -> bool {
        self.code == other.code && self.base_ty == other.base_ty
    }
}

impl PartialEq<Term> for Id {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Id(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Hash for Id {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.base_ty.hash(state)
    }
}

impl Code for Id {
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

#[cfg(test)]
mod test {}

/*!
Substitution of terms
*/
use super::*;

/// The trait implemented by substitution contexts
pub trait SubstCtx {
    /// Get a given index into the substitution stack
    fn ix(&self, ix: u64) -> Either<TermId, u64>;
    /// Add a binding
    fn push_binding(&mut self, arg_ty: TermId) -> Result<(), Error>;
    /// Remove a binding
    fn pop_binding(&mut self) -> Option<(TermId, u64)>;
}

/// A context for substituting terms
#[derive(Debug, Clone, Default)]
pub struct EvalCtx {
    /// The current stack of variable substitutions, along with their depths
    subst: Vec<(TermId, u64)>,
    /// The base shift
    base_shift: u64,
    /// The current depth
    depth: u64,
    /// Whether this substitution allows non-strictly matching types
    pub permissive: bool,
}

impl SubstCtx for EvalCtx {
    fn ix(&self, ix: u64) -> Either<TermId, u64> {
        if let Some(ix) = (self.subst.len() as u64).checked_sub(ix + 1) {
            let (term, depth) = &self.subst[ix as usize];
            let shift = self.depth - depth;
            Either::Left(term.shift(shift as i64, 0).expect("Shifting upwards should never fail").unwrap_or_else(|| term.clone()))
        } else {
            Either::Right((ix - self.subst.len() as u64) + self.base_shift)
        }
    }
    fn push_binding(&mut self, arg_ty: TermId) -> Result<(), Error> {
        self.depth += 1;
        self.base_shift += 1;
        let binding = Var::try_new(0, arg_ty)?.into_id();
        self.push(binding);
        Ok(())
    }
    fn pop_binding(&mut self) -> Option<(TermId, u64)> {
        self.depth -= 1;
        self.base_shift -= 1;
        self.pop()
    }
}

impl EvalCtx {
    /// Push a substitution onto the stack
    pub fn push(&mut self, subst: TermId) {
        self.subst.push((subst, self.depth))
    }
    /// Pop a substitution from the stack
    pub fn pop(&mut self) -> Option<(TermId, u64)> {
        self.subst.pop()
    }
    /// Get the current depth of this context
    pub fn depth(&self) -> u64 {
        self.depth
    }
    /// Get the base shift of this context
    pub fn base_shift(&self) -> u64 {
        self.base_shift
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_eval_test() {
        let nat = Nat.into_id();
        let n = num::BigUint::from(5u64).into_id();
        let var_set = Var::try_new(0, SET.into_id()).unwrap().into_id();
        let var_nat = Var::try_new(0, nat.clone()).unwrap().into_id();
        let mut ctx = EvalCtx::default();
        let triples = [
            (nat.clone(), nat.clone(), None),
            (nat.clone(), var_set, Some(nat.clone())),
            (n.clone(), var_nat, Some(n.clone())),
            (nat.clone(), n.clone(), None),
        ];
        for (value, input, output) in triples.iter().cloned() {
            ctx.push(value.clone());
            let subst = input.eval(&mut ctx).unwrap();
            assert_eq!(subst, output);
            assert_eq!(ctx.pop(), Some((value, 0)))
        }
    }
    #[test]
    fn failed_eval_test() {
        let nat = Nat.into_id();
        let n = num::BigUint::from(5u64).into_id();
        let set = SET.into_id();
        let var_set = Var::try_new(0, set.clone()).unwrap().into_id();
        let var_nat = Var::try_new(0, nat.clone()).unwrap().into_id();
        let mut ctx = EvalCtx::default();
        let pairs = [
            (set.clone(), var_set.clone()),
            (set.clone(), var_nat.clone()),
            (n.clone(), var_set.clone()),
        ];
        for (value, input) in pairs.iter().cloned() {
            ctx.push(value.clone());
            input.eval(&mut ctx).unwrap_err();
            assert_eq!(ctx.pop(), Some((value, 0)))
        }
    }
}

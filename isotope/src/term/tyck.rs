/*!
Type checking utilities
*/
use super::*;
use fxhash::FxHashMap;
use std::collections::hash_map::Entry;

/// A type-checking context
#[derive(Debug, Clone, Default)]
pub struct TyCtx {
    /// A map of bound variable indices to their required types
    bound_index_tys: Vec<TermId>,
    /// A map of free indices (by their index *at depth 0*) to their required types
    free_index_tys: FxHashMap<u64, TermId>,
}

impl TyCtx {
    /// Push a required type to the stack. Return an error if `ty` is not a type.
    #[inline]
    pub fn try_push(&mut self, ty: TermId) -> Result<(), Error> {
        if !ty.is_ty() {
            return Err(Error::NotAType);
        }
        self.bound_index_tys.push(ty);
        Ok(())
    }
    /// Push a required type to the stack, without checking if it is a type
    #[inline]
    pub fn push(&mut self, ty: TermId) {
        self.bound_index_tys.push(ty);
    }
    /// Pop a required index from the stack
    #[inline]
    pub fn pop(&mut self) -> Option<TermId> {
        self.bound_index_tys.pop()
    }
    /// Whether this context is null relative to a given term
    #[inline]
    pub fn is_null<V: Value>(&self, term: &V) -> bool {
        term.fvb() == 0 || (self.bound_index_tys.is_empty() && self.free_index_tys.is_empty())
    }
    /// Get the type constraint for a given index, if any
    #[inline]
    pub fn constraint(&self, ix: u64) -> Option<&TermId> {
        let bound_depth = self.bound_index_tys.len() as u64;
        if ix >= bound_depth {
            let ix = ix - bound_depth;
            self.free_index_tys.get(&ix)
        } else {
            Some(&self.bound_index_tys[(bound_depth - (ix + 1)) as usize])
        }
    }
    /// Check that a given type is compatible with a given index
    #[inline]
    pub fn check_compatible(&self, ix: u64, ty: &TermId) -> bool {
        let bound_depth = self.bound_index_tys.len() as u64;
        if ix >= bound_depth {
            let ix = ix - bound_depth;
            if let Some(curr_ty) = self.free_index_tys.get(&ix) {
                curr_ty == ty
            } else {
                true
            }
        } else {
            self.bound_index_tys[(bound_depth - (ix + 1)) as usize] == *ty
        }
    }
    /// Type an index. Return if constraints were updated, or an error on incompatibility
    #[inline]
    pub fn constrain(&mut self, ix: u64, ty: &TermId) -> Result<bool, Error> {
        let bound_depth = self.bound_index_tys.len() as u64;
        if ix >= bound_depth {
            let ix = ix - bound_depth;
            match self.free_index_tys.entry(ix) {
                Entry::Vacant(v) => {
                    v.insert(ty.clone());
                    Ok(true)
                }
                Entry::Occupied(o) if o.get() != ty => Err(Error::TypeMismatch),
                _ => Ok(false),
            }
        } else if self.bound_index_tys[(bound_depth - (ix + 1)) as usize] != *ty {
            Err(Error::TypeMismatch)
        } else {
            Ok(false)
        }
    }
    /// Get the current bound depth
    #[inline]
    pub fn bound_depth(&self) -> u64 {
        self.bound_index_tys.len() as u64
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_tyck_test() {
        let nat = Nat.into_id();
        let bool = Finite(2).into_id();
        let mut ctx = TyCtx::default();
        assert_eq!(ctx.bound_depth(), 0);
        assert!(ctx.check_compatible(0, &nat));
        assert!(ctx.check_compatible(0, &bool));
        ctx.push(nat.clone());
        assert_eq!(ctx.bound_depth(), 1);
        assert!(ctx.check_compatible(0, &nat));
        assert!(!ctx.check_compatible(0, &bool));
        assert!(!ctx.constrain(0, &nat).unwrap());
        ctx.constrain(0, &bool).unwrap_err();
        assert!(ctx.constrain(2, &nat).unwrap());
        assert!(!ctx.constrain(2, &nat).unwrap());
        ctx.constrain(2, &bool).unwrap_err();
        assert_eq!(ctx.constraint(0), Some(&nat));
        assert_eq!(ctx.constraint(1), None);
        assert_eq!(ctx.constraint(2), Some(&nat));
        assert_eq!(ctx.pop().unwrap(), nat);
        assert_eq!(ctx.constraint(0), None);
        assert_eq!(ctx.constraint(1), Some(&nat));
        assert_eq!(ctx.constraint(2), None);
        assert_eq!(ctx.bound_depth(), 0);
        assert!(ctx.check_compatible(0, &nat));
        assert!(ctx.check_compatible(0, &bool));
        ctx.try_push(nat.clone()).unwrap();
        assert_eq!(ctx.bound_depth(), 1);
        assert!(ctx.check_compatible(0, &nat));
        assert!(!ctx.check_compatible(0, &bool));
        ctx.try_push(Finite(2).ix(0).into_id()).unwrap_err();
    }
    #[test]
    fn incompatible_variables_do_not_typecheck() {
        let mut ctx_xy = TyCtx::default();
        let mut ctx_yx = TyCtx::default();
        let x = Var::try_new(0, SET.into_id()).unwrap();
        let y = Var::try_new(0, Nat.into_id()).unwrap();
        assert!(x.tyck_with(&mut ctx_xy));
        assert!(!y.tyck_with(&mut ctx_xy));
        assert!(y.tyck_with(&mut ctx_yx));
        assert!(!x.tyck_with(&mut ctx_yx));
    }
}

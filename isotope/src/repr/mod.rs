/*!
Typing for `isotope` RVSDG nodes
*/
use crate::prelude::*;

/// A representation for an `isotope` RVSDG node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Type {
    /// A finite type, with a compiler-defined runtime representation
    Finite(Finite),
    /// An abstract type, with *no* runtime representation
    Abstract(Abstract),
}

/// An abstract type, with *no* runtime representation
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Abstract(pub TermId);

/// Perform the same computation for every member of the `Type` enum
#[macro_export]
macro_rules! for_type {
    ($v:expr; $t:ident => $e:expr) => {
        match $v {
            Type::Finite($t) => $e,
            Type::Abstract($t) => $e,
        }
    };
}
/// A trait implemented by types for `isotope` RVSDG nodes
pub trait Repr {
    /// Get the denotation for this type as an `isotope` term
    fn denote(&self) -> CTerm;
}

impl Repr for Type {
    #[inline]
    fn denote(&self) -> CTerm {
        for_type!(self; ty => ty.denote())
    }
}

impl Repr for Abstract {
    #[inline]
    fn denote(&self) -> CTerm {
        CTerm::Id(&self.0)
    }
}
